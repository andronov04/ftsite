import FFmpeg from "./FFmpeg";


class CanvasRecorder {
  constructor(canvas, uuid='b8f876bd-c3b6-4190-b6ae-62c9da557184') {
    this.canvas = canvas;
    this.uuid = uuid;
    this.recorder = null;
    this.chunks = [];
  }

  start = async() => {
    const stream = this.canvas.captureStream();

    this.recorder = new MediaRecorder(stream);

    this.recorder.ondataavailable = e => this.chunks.push(e.data);

    this.recorder.onstop = e => this.onStop();

    this.recorder.start();
  };

  stop = async() => {
    this.recorder.stop();
  };

  onStop = () => {
    let blob = new Blob(this.chunks, {type: 'video/webm'});
    const vid = document.createElement('video');
    vid.src = URL.createObjectURL(blob);
    vid.controls = true;
    document.body.appendChild(vid);
    const a = document.createElement('a');
    a.download = `${this.uuid}.webm`;
    a.href = vid.src;
    document.body.appendChild(a);

    // Download
    let url = window.URL.createObjectURL(blob);
    a.click();
    window.URL.revokeObjectURL(url);


    // console.log('FFmpeg');
    // let ff = new FFmpeg();
    // // ff.initial();
    // ff.convertStreams(blob);
  };
}

export default CanvasRecorder;