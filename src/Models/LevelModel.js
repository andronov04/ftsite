
import {action, observable} from "mobx";
import LevelDataModel from "./LevelDataModel";
import LevelTipModel from "./LevelTipModel";
import LevelAnswerModel from "./LevelAnswerModel";

export default class LevelModel {
  data = null; // LevelDataModel
  id = null;
  key;
  tips = []; // LevelTipModel
  answers = []; // LevelAnswerModel
  @observable isAnswered = false;
  @observable isAsAnswered = null;
  sort = 0;
  @observable colors = [];
  shortInfo;
  fact;
  slug;

  constructor(jsonData) {
    let { backgroundColor, cameraPosition, elements, points, color } = jsonData.data;

    this.data = new LevelDataModel(backgroundColor, cameraPosition, elements, points, color);
    this.id = jsonData.id;
    this.tips = [];//jsonData.tips.map(item=> new LevelTipModel(item.title, item.tip, item.sort));
    this.answers = jsonData.answers.map((item, i)=> new LevelAnswerModel(item, item === jsonData.correct_answer, i));
    this.sort = jsonData.sort;
    this.key = jsonData.key;
    this.colors = jsonData.colors;
    this.shortInfo = jsonData.shortInfo;
    this.fact = jsonData.fact;
    this.slug = jsonData.slug;
  }

  @action setAnswer = (answer) => {
    this.isAnswered = true;
    this.isAsAnswered = answer.correct;
  };

}