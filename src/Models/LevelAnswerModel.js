
import { observable } from "mobx";

export default class LevelAnswerModel {
  id = Math.random();
  @observable title;
  correct = false;
  sort = 0;

  constructor(title, correct, sort) {
    this.title = title;
    this.correct = correct;
    this.sort = sort;
  }
}