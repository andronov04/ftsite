import { observable, computed, action, runInAction } from "mobx";
import LevelModel from "./LevelModel";
import * as dataLeague from "../data/league.json";
import LeagueModel from "./LeagueModel";
import GameService from "../Services/GameService";

const LANGUAGES = [
  'en',
  'es',
  'it',
  'de',
  'fr',
  'ru'
];

export default class GameModel {
  gameService = new GameService();
  @observable leagues = [];

  @observable currentLeague; //todo WIP
  @observable currentLevelID;
  @observable currentLevel;

  @observable isStart = false; ///todo change
  @observable isFinish = false;
  @observable lang = null;

  @observable isOpenGoalModal = false;

  @observable formSchema = null;

  @observable leagueColor = [
    "#ed248e",
    "#8f1755"
  ];
  leagueColors = {
    "apl": [
      "#4a907c",
      "#427a8f"
    ],
    "lal": [
      "#cc5c26",
      "#8f5d3c"
    ],
    "as": [
      "#ed248e",
      "#8f1755"
    ],
    "frl": [
      "#050090",
      "#232b8f"
    ],
    "bl": [
      "#834c90",
      "#8f0d85"
    ]
  };
  levelIndex = null;

  @observable isOpenShareModal = false;

  @action setCurrentLevel = (levelID) => {
    this.currentLevel = this.currentLeague.levels.find(a=>a.id === parseInt(levelID));
    this.currentLevelID = levelID;
  };

  @action setCurrentLeague = (league) => {
    this.currentLeague = league;
    league.levels.length && this.setCurrentLevel(league.levels[0].id);
  };

  initialLeague = (items) => {
    this.leagues = items.map(item=> new LeagueModel(item));
  };

  @action defaultCurrentLevel = () => {
    this.currentLevel = this.leagues[0].levels[0];
  };l

  @action nextLevel = () => {
    let currentIndex = this.levelIndex;
    if(currentIndex+1 < this.currentLeague.levels.length){
      this.currentLevel = this.currentLeague.levels[currentIndex+1]
    }
    else{
      this.isFinish = true
    }
    this.levelIndex += 1;
  };

  @action
  toHome = async() => {
    this.isStart = false;
  };

  @action
  changeGoalModal = ()  => {
    this.isOpenGoalModal = !this.isOpenGoalModal
  };

  prevLevel = async() => {
    let currentIndex = this.levelIndex;
    return this.leagues[0].levels[currentIndex-1]
  };

  @action changeLang = (lang) => {
    if(lang){
      this.lang = LANGUAGES.includes(lang) ? lang : 'en';
      return
    }
    let index = LANGUAGES.indexOf(this.lang);
    this.lang = index+1 === LANGUAGES.length ? LANGUAGES[0] : LANGUAGES[index+1];
    // document.title = this.lang === 'en' ? 'Who Scored The Goal?' : 'Кто забил этот гол?'
  };

  @action goGame = (league) => {
    console.log('goGame', league);
    this.currentLeague = league;
    this.currentLevel = league.levels[0]; // todo sort
    this.isStart = true
  };

  @action setColor = (leagueKey) => {
    this.leagueColor = this.leagueColors[leagueKey];
  };

  @action changeShareModal = (action) => {
    this.isOpenShareModal = action
  };


  getLeaguesAsync = async () => {
    // TODO need do it
    try {
      let params = {
        // pageNumber: this.countryData.pageNumber,
        // searchQuery: this.searchQuery,
        // isAscending: this.countryData.isAscending
      };
      const urlParams = new URLSearchParams(Object.entries(params));
      const data = await this.gameService.get('leagues', urlParams);
      runInAction(() => {
        console.log('YES', data);
        this.initialLeague(data.items);
      });
    } catch (error) {
      runInAction(() => {
        console.log('NO');
        // this.status = "error";
      });
    }
  };

  getSchemaAsync = async () => {
    let params = {
      // pageNumber: this.countryData.pageNumber,
      // searchQuery: this.searchQuery,
      // isAscending: this.countryData.isAscending
    };
    const urlParams = new URLSearchParams(Object.entries(params));
    const data = await this.gameService.get('schema', urlParams);
    runInAction(() => {
      console.log('YES', data);
      this.formSchema = data.schema
    });
  };

  getGoalsAsync = async () => {
    // TODO need do it
    try {
      let params = {
        // pageNumber: this.countryData.pageNumber,
        // searchQuery: this.searchQuery,
        // isAscending: this.countryData.isAscending
      };
      const urlParams = new URLSearchParams(Object.entries(params));
      const data = await this.gameService.get('goals', urlParams);
      runInAction(() => {
        this.leagues.forEach(league=>{
          league.initialLevels(data.items.filter(a=>a.leagueID === league.id))
        })
        // this.initialLeague(data.items);
        // this.countryData = data;
      });
    } catch (error) {
      runInAction(() => {
        console.log('NO GOAL');
        // this.status = "error";
      });
    }
  };

  createGoal = async (data) => {

    const result = await this.gameService.post('goal', data);
    runInAction(() => {
      // todo need any league
      this.currentLeague.addGoal(result.goal)
    });
  }
}