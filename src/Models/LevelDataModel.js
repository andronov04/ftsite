
import {action, observable} from "mobx";

export default class LevelDataModel {
  id = Math.random();
  backgroundColor = null;
  cameraPosition = [];
  elements = [];
  @observable points = [];
  color = {
    field: "#fff",
    ball: "#fff",
    line: "#fff",
    player: "#fff",
    rival: "#fff",
    point: "#fff"
  };

  constructor(backgroundColor, cameraPosition, elements, points, color) {
    this.backgroundColor = backgroundColor;
    this.cameraPosition = cameraPosition;
    this.elements = elements;
    this.points = points;
    this.color = color || this.color;
  }

  @action addPoint = (point) => {
    this.points.push(point);
    return point
  };
}