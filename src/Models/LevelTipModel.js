
import {action, observable} from "mobx";
import LevelModel from "./LevelModel";

export default class LevelTipModel {
  id = Math.random();
  @observable title;
  tip = null;
  sort = 0;
  @observable use = false;

  constructor(title, tip, sort) {
    this.title = title;
    this.tip = tip;
    this.sort = sort;
  }

  @action setUse = (use = false) => {
    this.use = use
  };
}