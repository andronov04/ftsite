
import {action, computed, observable} from "mobx";
import LevelModel from "./LevelModel";
import * as data from "../data/level.json";

/**
 * Leagues & Cups

 UK
 Premier League
 Championship

 SP
 La liga

 IT
 A Seria

 FR
 France

 GE
 Bundesliga

 RU
 Russian premier league


 Europe
 Champions League
 Europa League


 International
 World Cup
 * **/

export default class LeagueModel {
  title;
  uuid;
  id;
  key;
  keyTitle;

  @observable levels = [];
  @observable currentLevel;
  @observable isFinishLeague = false;

  @computed
  get levelIndex() {
    return this.levels.indexOf(this.currentLevel)
  }
  constructor(leagueData) {
    this.id = leagueData.id;
    this.title = leagueData.title;
    this.uuid = leagueData.uuid;
    this.key = leagueData.key;
    this.keyTitle = leagueData.keyTitle;
  }

  @action initialLevels = (items) => {
    // TODO from json
    // for tests .filter(a=>a.leagueUUID && a.leagueUUID === this.uuid)
    this.levels = items.map(item=> new LevelModel(item));
    // default first level

    this.currentLevel = this.levels[0];
  };

  @action addGoal = (item) => {
    let goal = new LevelModel(item);
    this.levels.push(goal)
  };

  @action restartLevels = async() => {
    this.isFinish = false;
    this.levels.forEach(item=>{
      item.isAsAnswered = null;
      item.isAnswered = false;
      item.tips.forEach(tip=>{
        tip.use = false
      })
    });
    this.currentLevel = this.levels[0]
  };
}