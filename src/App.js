import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css';
import HomeView from "./components/HomeView";
import EditorView from "./components/Editor/EditorView";
import GIFView from "./components/GIF/GIFView";
import {Provider} from "mobx-react";
import MediaView from "./components/Media/MediaView";
import PrivacyPolicy from "./components/PrivacyPolicy/PrivacyPolicy";
import SupportView from "./components/Support/SupportView";

class App extends Component {

  render() {
    return (
      <Router>
        <Provider store={this.props.store}>
        <div className="App">
          <Route exact path="/" component={HomeView} />

          <Route path="/editor" component={EditorView} />
          <Route path="/gif" component={GIFView} />
          <Route path="/media" component={MediaView} />
          <Route path="/privacy-policy" component={PrivacyPolicy} />
          <Route path="/support" component={SupportView} />
        </div>
        </Provider>
      </Router>
    );
  }
}

export default App;
