import {Component} from "react";
import React from "react";
import * as THREE from "three";
import OrbitControlsClass from "../../lib/OrbitControls";
import Game from "../../Game/Game";
import {TimelineMax} from "gsap/TweenMax";
import BaseView from "../Base/BaseView";
import {inject, observer} from "mobx-react";
import {reaction} from "mobx";

@inject("store")
@observer
class AnimationView extends Component {
  raycaster = new THREE.Raycaster();

  leagueColor = [
    "#ed248e",
    "#8f1755"
  ];

  componentDidMount = async() =>{
    // INIT MOBX
    this._notificationsReactionDispose = reaction(
      () => this.props.store.currentLevel,
      async (currentLevel, reaction) => {
        //console.log('MOBX', currentLevel, reaction);

        await this.nextGoal()

      }
    );

    let {currentLevel} = this.props.store;

    const width = this.mount.clientWidth;
    const height = this.mount.clientHeight;
    //ADD SCENE
    this.scene = new THREE.Scene();
    window.scene = this.scene;
    this.camera = new THREE.PerspectiveCamera( 60, width / height, 0.1, 10000 );
    window.camera = this.camera;
    this.camera.position.set(...currentLevel.data.cameraPosition);
    //ADD RENDERER
    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    //  this.renderer.setClearColor('#0e450e');
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize(width, height);
    this.mount.appendChild(this.renderer.domElement);
    //await this.setText(this.scene);

    this.controls = new OrbitControlsClass.OrbitControls(this.camera, this.renderer.domElement );
    this.controls.maxPolarAngle = Math.PI / 2 - 0.1;
    //controls.minPolarAngle = 2;
    this.controls.minDistance = 0;
    this.controls.maxDistance = 2000;
    this.controls.rotateSpeed = 0.2;

    window.addEventListener( 'resize', this.onWindowResize, false );

    //this.start();
    let DT = JSON.parse(JSON.stringify(currentLevel.data));
    await this.startLevel(this.scene, this.camera, this.renderer, this.controls, DT);

    // TODO test color
    this.mount.style.backgroundImage = `linear-gradient(to top, ${currentLevel.colors[0]} 0%, ${currentLevel.colors[1]} 100%)`;
  };

  startLevel = async (scene, camera, renderer, controls, data) => {

    let game = new Game(scene, camera, renderer, controls);
    // await game.initField();
    await game.initGame(data.color.field);


    // callbacks
    await game.level.setupCallback('onFinishGoal', this.onFinishGoal);
    await game.level.setupCallback('onCompleteGoal', this.onCompleteGoal);

    await game.start(data);


    AnimationView.game = game;
    console.log('Init game!');
  };

  nextGoal = async() => {
    let { prevLevel, currentLevel } = this.props.store;

    // Change level data
    await AnimationView.game.level.onFinishGoal().then(async () => {

    });


    // Change background color
    let _prevLevel = await prevLevel();
    let colors = {top:_prevLevel.colors[0], bottom:_prevLevel.colors[1]};

    new TimelineMax().to(colors, 3, {
      top:currentLevel.colors[0],
      bottom:currentLevel.colors[1],
      onUpdateParams:[this.mount],
      onUpdate:(element)=>{
        element.style.backgroundImage = "linear-gradient(to top," + colors.top + ", " + colors.bottom + ")";
      }
    });

  };

  changeView = async() => {
    console.log('changeView')
  };

  onCompleteGoal = async() => {
    console.log('onCompleteGoal-onCompleteGoal');
    await AnimationView.game.level.onFinishGoal()
  };

  onFinishGoal = async() => {
    console.log('onFinishGoal-onFinishGoal');
    let DT = JSON.parse(JSON.stringify(this.props.store.currentLevel.data));
    await AnimationView.game.start(DT);

  };

  onClickCanvas = async () => {
    // await this.onCompleteGoal();
    // await this.onFinishGoal()
  };

  onWindowResize = () =>{
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  };


  onMouseDown = (evt) => {
    let mouse = new THREE.Vector2();
    mouse.x = ((evt.clientX) / this.renderer.domElement.width) * 2 - 1;
    mouse.y = -((evt.clientY) / this.renderer.domElement.height) * 2 + 1;

    this.raycaster.setFromCamera(mouse, this.camera);
  };

  componentDidUpdate = async(prevProps, prevState) => {
    // todo refactoring use a first level for this league
    if(this.leagueColor !== this.props.store.leagueColor){
      let leagueColor = this.props.store.leagueColor;

      let colors = {top:this.leagueColor[0], bottom:this.leagueColor[1]};

      new TimelineMax().to(colors, 1, {
        top:leagueColor[0],
        bottom:leagueColor[1],
        onUpdateParams:[this.mount],
        onUpdate:(element)=>{
          element.style.backgroundImage = "linear-gradient(to top," + colors.top + ", " + colors.bottom + ")";
        }
      });
      this.leagueColor =  leagueColor;

    }
  };

  render(){
    let {currentLevel: level, nextLevel, isStart, classLeague, leagueColor} = this.props.store;

    return([
        <div
          key={1}
          onClick={this.onClickCanvas}
          className={`layer ${classLeague}`}
          style={{ width: '100%', height: '100%',
            position: 'absolute',
            opacity: !isStart ? .2 : 1
            // backgroundImage: `linear-gradient(to top, ${level.colors[0]} 0%, ${level.colors[1]} 100%)`
          }}
          onMouseDown={this.onMouseDown}
          ref={(mount) => { this.mount = mount }}
        />,
        <BaseView key={2} changeView={this.changeView}/>
      ]
    )
  }
}

export default AnimationView;