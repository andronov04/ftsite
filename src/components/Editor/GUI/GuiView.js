import {Component} from "react";
import React from "react";
import * as dg from 'dis-gui';
import {inject, observer} from "mobx-react"
import GoalModalView from "../Modal/GoalModalView";

const KIND_LINE = ['linePass', 'lineCross', 'lineRun'];


@inject("store")
@observer
class GuiView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: '#000000',
      currentLevelID: null,
      level: {
        id: 1,
        data: {
          elements: [],
          cameraPosition:[
            -309.26041693504686,
            589.7815303691531,
            738.1110852651293
          ],
          points: [],
        }
      }
    };
  }

  editorSetState = async(points)=>{
    // this.setState({
    //   level: {
    //     data:{
    //       points: [...points]
    //     }
    //   }
    // });
  };

  // componentDidUpdate =(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) => {
  //   if(prevProps.editor === undefined && this.props.editor){
  //     this.props.editor.setupSetState(this.editorSetState)
  //   }
  // };



  componentDidUpdate = (prevProps, prevState) => {
    // if(prevProps.editor === undefined && this.props.editor){
    //   this.props.editor.setupSetState(this.editorSetState)
    // }
    //
    console.log(this)
    this.editor = this.props.getEditor();
    if(!this.editor)return
    // let propCurrentLevelID = this.props.store.currentLevelID;
    // if(propCurrentLevelID !== this.state.currentLevelID && this.props.store.currentLevel){
    //   this.setState({
    //     // level: this.props.editor.levelToEditor(this.props.store.currentLevel),
    //     currentLevelID: propCurrentLevelID
    //   })
    //
    this.props.store.currentLevel && this.editor.levelToEditor(this.props.store.currentLevel)
    //
    // }
  };

  renderLabelPoint(item) {

    return(
      <div>
        <div style={{
          // backgroundColor: item.instance.material.hexColor, TODO need done
          width: '10px',
          height: '10px',
          borderRadius: '100px',
          marginRight: '10px',
          display: 'inline-block'
        }}/>
        {`Point №${item.id}`}
      </div>
    )
  }

  render() {
    // const {
    //   level
    // } = this.state;

    const {
      // editor,
      isEditor,
      store
    } = this.props;

    let editor = this.props.getEditor();

    // if(!editor && !isEditor){
    //   return(null)
    // }

    const { leagues, currentLevel: level, currentLeague, setCurrentLeague, setCurrentLevel, changeGoalModal } = store;

    return (
      <div className="gui">
        <div className="gui__block">
          <dg.GUI
            style={{height: '100%', zIndex: 10}}
          >

            <dg.Select
              label='League'
              onChange={(item)=>{
                let l = leagues.find(a=>a.id === parseInt(item.split('-')[0]));
                setCurrentLeague(l);
              }}
              options={leagues.map(item=>`${item.id}-${item.key}`)}
            />

            {/*<dg.Folder key={'addgoal'} label={'Add a goal'} expanded={false}>*/}
            {/*</dg.Folder>*/}
            {currentLeague && <dg.Button
              label='Add goal'
              onClick={()=>{changeGoalModal()}}
            />}

            {currentLeague && <dg.Select
              label='Goal'
              onChange={(item)=>{
                setCurrentLevel(item)
              }}
              options={currentLeague.levels.reverse().map(item=>`${item.id}`)}
            />}

            {level && <dg.Checkbox
              key={`bl_false`}
              label='Need update'
              checked={false}
              // onChange={editor.changeBall.bind(this, item)}
            />}

            <dg.Button
              label='Save the goal'
              onClick={editor && editor.toLevel.bind(this, true)}
            />

            <dg.Button
              label='Update preview'
              onClick={editor && editor.toLevel.bind(this)}
            />

            <dg.Button
              label='Switch view 3D/2D'
              onClick={editor && editor.switchView.bind(this)}
            />

            {/*<dg.Folder key={'color'} label={'Base'} expanded={true}>*/}
            {/*  <ModalView />*/}
            {/*</dg.Folder>*/}


            <dg.Folder label='Points' expanded={true}>
              {level && level.data.points.map((item, i)=>
                <dg.Folder key={item.id}
                           label={this.renderLabelPoint(item)}
                           expanded={item.isActive}>
                  {/*{console.log('PpP', item)}*/}
                  {<dg.Checkbox
                    key={`pl_${item.id}`}
                    label='Player'
                    checked={false}
                    // onChange={editor.changePlayer.bind(this, item)}
                  />}

                  {<dg.Checkbox
                    key={`rl_${item.id}`}
                    label='Rival'
                    checked={false}
                    // onChange={editor.changeRival.bind(this, item)}
                  />}

                  {<dg.Checkbox
                    key={`bl_${item.id}`}
                    label='Ball'
                    checked={!!item.ball}
                    // onChange={editor.changeBall.bind(this, item)}
                  />}

                  {<dg.Checkbox
                    key={`st_${item.id}`}
                    label='Static element'
                    checked={true}
                    // onChange={editor.changeStatic.bind(this, item)}
                  />}

                  {/*{item.line  && <dg.Select label='Type direction'*/}
                  {/*            value={item.line.userData.kind}*/}
                  {/*            onChange={editor.changeKindLine.bind(this, item)}*/}
                  {/*            options={KIND_LINE}*/}
                  {/*/>}*/}

                  {<dg.Number
                    label='Speed '
                    // onChange={editor.changeDurationTime.bind(this, item)}
                    value={item.durationTime}/>}

                  {<dg.Number
                    label='Start time '
                    // onChange={editor.changeStartTime.bind(this, item)}
                    value={item.startTime}/>}

                  {<dg.Button
                    key={`line_${item.id}`}
                    label={`Add Line`}
                    // onClick={editor.addLine.bind(this, item)}
                  />}

                  <dg.Button
                    key={`rmp_${item.id}`}
                    label={`Remove`}
                    onClick={function() {console.log('remove')}}
                  />
                </dg.Folder>
              )}
              <dg.Button
                label='Add point'
                onClick={()=> {
                  editor.addPoint().then(point=>{
                    level.data.addPoint(point)
                  })
                }}
                // onClick={editor && editor.addPoint.bind(this)}
              />
            </dg.Folder>

            {/*<dg.Folder label='Directions' expanded={true}>
              <dg.Button
                label='Add direction'
                onClick={function() { editorCallback('addLine')}}
              />
            </dg.Folder>*/}

          </dg.GUI>
        </div>
      </div>
    );
  }
}

export default GuiView ;