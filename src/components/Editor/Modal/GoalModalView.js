import React, {Component} from 'react';
import Form from "react-jsonschema-form";
import Popup from "reactjs-popup";
import {inject, observer} from "mobx-react";


const log = (type) => console.log.bind(console, type);

@inject("store")
@observer
class GoalModalView extends Component {

  componentWillMount = async() => {
    if(!this.props.store.formSchema){
      await this.props.store.getSchemaAsync();
    }
  };

  submitForm = async(data) => {
    console.log('submitForm', data.formData);
    await this.props.store.createGoal(JSON.stringify(data.formData))
  };

  render() {


    const {
      isOpenGoalModal,
    } = this.props;

    const {
      formSchema
    } = this.props.store;

    return (
      <Popup
        overlayStyle={{overflow: 'auto'}}
        open={isOpenGoalModal}
        // trigger={<button className="button"> Open Modal </button>}
        // modal
        // closeOnDocumentClick
      >

        <div style={{zIndex: 100}}>
          {formSchema && <Form schema={formSchema}
                onChange={log("changed")}
                onSubmit={this.submitForm}
                onError={log("errors")} />
          }
        </div>
      </Popup>
    );
  }
}

export default GoalModalView;