import {Component} from "react";
import React from "react";
import * as THREE from "three";
import OrbitControlsClass from "../../lib/OrbitControls";
import Game from "../../Game/Game";
import {inject, observer} from "mobx-react"

let TEST_LEVEL = {
  data: {
    // "elements":[
    //   {
    //     "id":110,
    //     "type":"line",
    //     "kind":"linePass",
    //     "positions":[
    //       [
    //         81.38465183816564,
    //         0,
    //         755.6718835568483
    //       ],
    //       [
    //         191.36722828527093,
    //         0,
    //         712.5954891446771
    //       ],
    //       [
    //         268.25599466275804,
    //         0,
    //         655.0405421866946
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 1,
    //     "type":"line",
    //     "kind":"lineRun",
    //     "animateOrder":0,
    //     "speed":5,
    //     "objects":null,
    //     "class":"line",
    //     "positions":[
    //       [
    //         268.25599466275804,
    //         0,
    //         655.0405421866946
    //       ],
    //       [
    //         285.16224190958684,
    //         0,
    //         434.0847177018298
    //       ],
    //       [
    //         190.37676742651686,
    //         0,
    //         268.9746906343256
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 2,
    //     "type":"line",
    //     "kind":"lineCross",
    //     "animateOrder":1,
    //     "speed":9,
    //     "objects":null,
    //     "class":"line",
    //     "positions":[
    //       [
    //         129.8185730747511,
    //         0,
    //         91.88712508681448
    //       ],
    //       [
    //         50.89473980806213,
    //         0,
    //         50.229906683183799
    //       ],
    //       [
    //         -78.89473980806213,
    //         0,
    //         -14.229906683183799
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 3,
    //     "type":"line",
    //     "kind":"lineEmpty",
    //     "positions":[
    //       [
    //         50.8185730747511,
    //         0,
    //         91.88712508681448
    //       ],
    //       [
    //         150.89473980806213,
    //         0,
    //         125.229906683183799
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 101,
    //     "type":"player",
    //     "kind":"player",
    //     "animateOrder":0,
    //     "speed":8.5,
    //     "objects":null,
    //     "class":"player",
    //     "positions":[
    //       [
    //         93.85546991928351,
    //         0,
    //         685.9521384640199
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 102,
    //     "type":"player",
    //     "kind":"player",
    //     "positions":[
    //       [
    //         120.49468161466183,
    //         0,
    //         702.1046290706427
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 50,
    //     "type":"rival",
    //     "kind":"rival",
    //     "animateOrder":0,
    //     "speed":5.5,
    //     "objects":null,
    //     "class":"rival",
    //     "positions":[
    //       [
    //         110.73886476684902,
    //         0,
    //         675.4131940293175
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 51,
    //     "type":"rival",
    //     "kind":"rival",
    //     "animateOrder":0,
    //     "speed":5.5,
    //     "objects":null,
    //     "class":"rival",
    //     "positions":[
    //       [
    //         -45.73668881989465,
    //         0,
    //         689.8757575241475
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 52,
    //     "type":"rival",
    //     "kind":"rival",
    //     "animateOrder":0,
    //     "speed":5.5,
    //     "objects":null,
    //     "class":"rival",
    //     "positions":[
    //       [
    //         -7.353165982166999,
    //         0,
    //         382.2645990113729
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 53,
    //     "type":"rival",
    //     "kind":"rival",
    //     "animateOrder":0,
    //     "speed":5.5,
    //     "objects":null,
    //     "class":"rival",
    //     "positions":[
    //       [
    //         95.38171166927623,
    //         0,
    //         127.90161301705842
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 54,
    //     "type":"rival",
    //     "kind":"rival",
    //     "animateOrder":0,
    //     "speed":5.5,
    //     "objects":null,
    //     "class":"rival",
    //     "positions":[
    //       [
    //         243.8352767616858,
    //         0,
    //         377.7811766884606
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 55,
    //     "type":"rival",
    //     "kind":"rival",
    //     "animateOrder":0,
    //     "speed":5.5,
    //     "objects":null,
    //     "class":"rival",
    //     "positions":[
    //       [
    //         109.32617206598007,
    //         0,
    //         103.20493847575304
    //       ]
    //     ]
    //   },
    //   {
    //     "id": 56,
    //     "type":"rival",
    //     "kind":"rival",
    //     "animateOrder":0,
    //     "speed":5.5,
    //     "objects":null,
    //     "class":"rival",
    //     "positions":[
    //       [
    //         88.38126863963993,
    //         0,
    //         61.110184899223555
    //       ]
    //     ]
    //   },
    //   // {
    //   //   "id": 57,
    //   //   "type":"player",
    //   //   "kind":"player",
    //   //   "animateOrder":0,
    //   //   "speed":5.5,
    //   //   "objects":null,
    //   //   "class":"player",
    //   //   "positions":[
    //   //     [
    //   //       130.1815348190289,
    //   //       0,
    //   //       91.94293583175704
    //   //     ]
    //   //   ]
    //   // }
    // ],
    // points: [
    //   {
    //     id: 0,
    //     sort: 0,
    //     pos: [
    //       129.89473980806213,
    //       0,
    //       850.229906683183799
    //     ],
    //     point: 1,
    //     line: 110,
    //     player: 101,
    //     durationTime: 5,
    //     typeDirection: 'Ball',
    //     startTime: 0
    //   },
    //   {
    //     id: 1,
    //     sort: 1,
    //     pos: [
    //       93.24560711515244,
    //       0,
    //       686.0491234316434
    //     ],
    //     point: 2,
    //     line: 1,
    //     player: 101,
    //     ball: 0,
    //     durationTime: 5
    //   },
    //   {
    //     id: 2,
    //     sort: 2,
    //     pos: [
    //       129.9398903362528,
    //       0,
    //       90.89190588820941
    //     ],
    //     point: 3,
    //     line: 2,
    //     ball: 1,
    //     durationTime: 5,
    //   },
    // ],

    "elements":[],
    "points":[],
    "backgroundColor":"#b99b7b",
    "cameraPosition":[
      489.3029428582713,
      710.24205960331875,
      1024.966418708877
    ]
  },
  "id":0,
  "tips":[
    {
      "sort":0,
      "tip":"PLACE",
      "title":"Glasgow",
      "use":false
    },
    {
      "sort":1,
      "tip":"DATE",
      "title":"15/05/2005",
      "use":false
    },
    {
      "sort":2,
      "tip":"MINUTE",
      "title":"45'",
      "use":false
    }
  ],
  "answers":[
    {
      "title":"Carlos",
      "sort":0,
      "correct":false
    },
    {
      "title":"Henry",
      "sort":1,
      "correct":false
    },
    {
      "title":"Zidane",
      "sort":2,
      "correct":true
    },
    {
      "title":"Beckham",
      "sort":3,
      "correct":false
    }
  ],
  "isAnswered":false,
  "isAsAnswered":null,
  "sort":0,
  "shortInfo":"Champions League",
  "levelId":1
};



@inject("store")
@observer
class PreviewView extends Component {
  raycaster = new THREE.Raycaster();

  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: '#000000',
      level: TEST_LEVEL
    };

  }

  componentDidMount = async() =>{
    const width = this.mount.clientWidth;
    const height = this.mount.clientHeight;
    //ADD SCENE
    this.scene = new THREE.Scene();
    window.scene = this.scene;
    //ADD CAMERA
    // this.camera = new THREE.PerspectiveCamera(
    //   80,
    //   width / height,
    //   1,
    //   0 // !!!MAYBE important
    // );
    this.camera = new THREE.PerspectiveCamera( 60, width / height, 0.1, 10000 );
    window.camera = this.camera;
    this.camera.position.set(...this.state.level.data.cameraPosition);
    //ADD RENDERER
    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    //  this.renderer.setClearColor('#0e450e');
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize(width, height);
    this.mount.appendChild(this.renderer.domElement);
    //await this.setText(this.scene);

    this.controls = new OrbitControlsClass.OrbitControls(this.camera, this.renderer.domElement );
    // this.controls.maxPolarAngle = Math.PI / 2 - 0.1;
    // //controls.minPolarAngle = 2;
    // this.controls.minDistance = 0;
    // this.controls.maxDistance = 2000;
    // this.controls.rotateSpeed = 0.07;
    window.previewCamera = this.camera;

    window.addEventListener( 'resize', this.onWindowResize, false );

    //this.start();
    await this.startLevel(this.scene, this.camera, this.renderer, this.controls, TEST_LEVEL.data);
  };

  startLevel = async (scene, camera, renderer, controls, data) => {

    let game = new Game(scene, camera, renderer, controls);
    // await game.initField();
    await game.initGame();


    // SETUP EDITOR
    await this.props.editor.setupPreview(scene, camera, renderer, controls, game);
    // callbacks
    // await game.level.setupCallback('onFinishGoal', this.onFinishGoal);
    // await game.level.setupCallback('onCompleteGoal', this.onCompleteGoal);

    await game.start(data);


    PreviewView.game = game;
    console.log('Init game preview!');
  };

  componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
    // console.log('4', this.props.store.currentLevel, prevProps.store.currentLevel)

  };

  onWindowResize = () =>{
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  };


  onMouseDown = (evt) => {
    let mouse = new THREE.Vector2();
    mouse.x = ((evt.clientX) / this.renderer.domElement.width) * 2 - 1;
    mouse.y = -((evt.clientY) / this.renderer.domElement.height) * 2 + 1;

    this.raycaster.setFromCamera(mouse, this.camera);
  };

  render(){
    // <BaseView/>width: '320px', height: '568px'
    return(
      <div className={'preview'}>
        <div
          key={1}
          style={{ width: '320px', height: '568px',  bottom: '0px',
            position: 'absolute',
            backgroundImage: 'linear-gradient(to top, #a33193 0%, #ba202c 100%)'
          }}
          onMouseDown={this.onMouseDown}
          ref={(mount) => { this.mount = mount }}
        />
      </div>
    )
  }
}

export default PreviewView;