import {Component} from "react";
import React from "react";
import * as THREE from "three";
import OrbitControlsClass from "../../lib/OrbitControls";
import Game from "../../Game/Game";
import {TimelineMax} from "gsap/TweenMax";
import {inject, observer} from "mobx-react";
import GuiView from "./GUI/GuiView";
import PreviewView from "./PreviewView";
import Editor from "../../Editor/Editor";
import GoalModalView from "./Modal/GoalModalView";

@inject("store")
@observer
class EditorView extends Component {
  raycaster = new THREE.Raycaster();

  leagueColor = [
    "#ed248e",
    "#8f1755"
  ];

  constructor(props) {
    super(props);
    this.state = {
      isEditor: false,
      currentLevelID: null,
      backgroundColor: '#000000',
      level: { //todo delete
        id: 1,
        data: {
          elements: [],
          cameraPosition:[
            -309.26041693504686,
            589.7815303691531,
            738.1110852651293
          ],
          points: [],
        }
      }
    };
  }

  componentWillMount = async() => {
    await this.props.store.getLeaguesAsync();
    await this.props.store.getGoalsAsync();

    // this.props.store.initialLeague();
    this.props.store.setCurrentLeague(this.props.store.leagues[0]);
    // this.props.store.currentLeague.initialLevels();
    // this.props.store.defaultCurrentLevel();
  };

  initLevel = async() =>{
    // INIT MOBX

    let {currentLevel} = this.props.store;

    // if(!currentLevel){
    //   currentLevel = this.state.level;
    // }

    const width = this.mount.clientWidth;
    const height = this.mount.clientHeight;
    //ADD SCENE
    this.scene = new THREE.Scene();
    window.scene = this.scene;
    // this.camera = new THREE.PerspectiveCamera( 60, width / height, 0.1, 10000 );
    // window.camera = this.camera;
    // this.camera.position.set(...currentLevel.data.cameraPosition);

    this.camera = new THREE.PerspectiveCamera( 60, width / height, 0.1, 10000 );
    //    this.camera =new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 1, 10000 );
    window.camera = this.camera;

    //90*
    const horizontalFov = 90;
    this.camera.fov = (Math.atan(Math.tan(((horizontalFov / 2) * Math.PI) / 180) / this.camera.aspect) * 2 * 180) / Math.PI;
    // this.camera.rotation.y = 90 * Math.PI / 180;
    // this.camera.rotation.x = 90 * Math.PI / 180;
    // this.camera.rotation.z = 90 * Math.PI / 180;
    this.camera.position.set(
      0,
      1100,
      877
    );
    this.camera.updateProjectionMatrix();

    //ADD RENDERER
    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    //  this.renderer.setClearColor('#0e450e');
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize(width, height);
    this.mount.appendChild(this.renderer.domElement);
    //await this.setText(this.scene);

    this.controls = new OrbitControlsClass.OrbitControls(this.camera, this.renderer.domElement );
    this.controls.maxPolarAngle = THREE.Math.degToRad(-180);//Math.PI / 2 - 0.1;
    this.controls.minPolarAngle = THREE.Math.degToRad(-90);
    this.controls.minAzimuthAngle = 0;
    this.controls.maxAzimuthAngle = 0;
    this.controls.minDistance = 0;
    this.controls.maxDistance = 2000;
    this.controls.rotateSpeed = 0.07;

    window.addEventListener( 'resize', this.onWindowResize, false );

    //this.start();
    let DT = JSON.parse(JSON.stringify(currentLevel.data));
    await this.startLevel(this.scene, this.camera, this.renderer, this.controls, DT);

    // TODO test color
    // this.mount.style.backgroundImage = `linear-gradient(to top, ${currentLevel.colors[0]} 0%, ${currentLevel.colors[1]} 100%)`;
  };

  startLevel = async (scene, camera, renderer, controls, data) => {

    let game = new Game(scene, camera, renderer, controls);
    // await game.initField();
    await game.initGame();

    //Set Editor
    let editor =  new Editor(scene, camera, controls, renderer, this.raycaster);
    await editor.create();
    this.editor = editor;
    // this.setState(({isEditor: true}));

    // // callbacks
    await game.level.setupCallback('onFinishGoal', this.onFinishGoal);
    await game.level.setupCallback('onCompleteGoal', this.onCompleteGoal);

    await game.start(data);


    EditorView.game = game;
    EditorView.editor = editor;

    console.log('Init game!', this.props.store.currentLevel);
  };

  changeView = async() => {
    console.log('changeView')
  };

  onClickCanvas = async () => {
    // await this.onCompleteGoal();
    // await this.onFinishGoal()
  };

  onWindowResize = () =>{
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  };

  componentDidUpdate = async (prevProps, prevState) => {
    let propCurrentLevelID = this.props.store.currentLevelID;
    if(propCurrentLevelID !== this.state.currentLevelID && this.props.store.currentLevelID){

      await this.initLevel().then(item=>{
        this.setState({
          // level: this.props.store.currentLevel,
          currentLevelID: propCurrentLevelID,
          isEditor: true
        });
      })
      // this.setState({
      //   // level: this.props.store.currentLevel,
      //   currentLevelID: propCurrentLevelID
      // });

      // let DT = JSON.parse(JSON.stringify(this.props.store.currentLevel.data));
      // await this.startLevel(this.scene, this.camera, this.renderer, this.controls, DT);
    }
  };

  onMouseDown = (evt) => {
    let mouse = new THREE.Vector2();
    mouse.x = ((evt.clientX) / this.renderer.domElement.width) * 2 - 1;
    mouse.y = -((evt.clientY) / this.renderer.domElement.height) * 2 + 1;

    this.raycaster.setFromCamera(mouse, this.camera);
  };

  getEditor = () =>{
    console.log('this.editor', EditorView.editor, this.editor)
    return this.editor
  }

  render(){
    let {currentLevel: level, nextLevel, isStart, classLeague, leagueColor, isOpenGoalModal} = this.props.store;
    const { isEditor } = this.state;
    // console.log('LL', level, this.state.level);

    return([
        <div
          key={1}
          onClick={this.onClickCanvas}
          className={`layer ${classLeague}`}
          style={{ width: '100%', height: '102%',
            position: 'absolute',
            backgroundColor: 'black'
          }}
          onMouseDown={this.onMouseDown}
          ref={(mount) => { this.mount = mount }}
        />,
        <GuiView
         editor={this.editor}
         getEditor={this.getEditor}
         isEditor={this.state.isEditor}
        />,
        <div>{isOpenGoalModal && <GoalModalView isOpenGoalModal={isOpenGoalModal}/>}</div>
      ]
    )
  }
}

export default EditorView;
// import {Component} from "react";
// import React from "react";
// import * as THREE from "three";
// import OrbitControlsClass from "../../lib/OrbitControls";
// import Game from "../../Game/Game";
// import GuiView from "./GUI/GuiView";
// import Editor from "../../Editor/Editor";
// import DragControlsClass from "../../lib/DragControls";
// import TransformControlsClass from "../../lib/TransformControls";
// import uuid from "uuid";
// import PreviewView from "./PreviewView";
//
//
// class EditorView extends Component {
//   raycaster = new THREE.Raycaster();
//
//   constructor(props) {
//     super(props);
//     this.state = {
//       isEditor: false,
//       backgroundColor: '#000000',
//       level: { //todo delete
//         id: 1,
//         data: {
//           elements: [],
//           cameraPosition:[
//             -309.26041693504686,
//             589.7815303691531,
//             738.1110852651293
//           ],
//           points: [],
//         }
//       }
//     };
//   }
//
//   componentDidMount = async() =>{
//     const width = this.mount.clientWidth;
//     const height = this.mount.clientHeight;
//     //ADD SCENE
//     this.scene = new THREE.Scene();
//     window.scene = this.scene;
//     //ADD CAMERA
//     // this.camera = new THREE.PerspectiveCamera(
//     //   80,
//     //   width / height,
//     //   1,
//     //   0 // !!!MAYBE important
//     // );
//     this.camera = new THREE.PerspectiveCamera( 60, width / height, 0.1, 10000 );
//     //    this.camera =new THREE.OrthographicCamera( width / - 2, width / 2, height / 2, height / - 2, 1, 10000 );
//     window.camera = this.camera;
//
//     //90*
//     const horizontalFov = 90;
//     this.camera.fov = (Math.atan(Math.tan(((horizontalFov / 2) * Math.PI) / 180) / this.camera.aspect) * 2 * 180) / Math.PI;
//     // this.camera.rotation.y = 90 * Math.PI / 180;
//     // this.camera.rotation.x = 90 * Math.PI / 180;
//     // this.camera.rotation.z = 90 * Math.PI / 180;
//     this.camera.position.set(
//       0,
//       1100,
//       877
//     );
//     this.camera.updateProjectionMatrix();
//
//     var helper = new THREE.CameraHelper( this.camera );
//     this.scene.add( helper );
//
//     //ADD RENDERER
//     this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
//     //  this.renderer.setClearColor('#0e450e');
//     this.renderer.setPixelRatio( window.devicePixelRatio );
//     this.renderer.setSize(width, height);
//     this.mount.appendChild(this.renderer.domElement);
//     //await this.setText(this.scene);
//
//     this.controls = new OrbitControlsClass.OrbitControls(this.camera, this.renderer.domElement );
//     this.controls.maxPolarAngle = THREE.Math.degToRad(-180);//Math.PI / 2 - 0.1;
//     this.controls.minPolarAngle = THREE.Math.degToRad(-90);
//     this.controls.minAzimuthAngle = 0;
//     this.controls.maxAzimuthAngle = 0;
//     this.controls.minDistance = 0;
//     this.controls.maxDistance = 2000;
//     this.controls.rotateSpeed = 0.07;
//
//    // this.initDragAndDrop();
//
//     window.addEventListener( 'resize', this.onWindowResize, false );
//
//     //this.start();
//     await this.startLevel(this.scene, this.camera, this.renderer, this.controls, this.state.level.data);
//   };
//
//   startLevel = async (scene, camera, renderer, controls, data) => {
//
//     let game = new Game(scene, camera, renderer, controls);
//     // await game.initField();
//     await game.initGame();
//
//     //Set Editor
//     let editor =  new Editor(scene, camera, controls, renderer);
//     await editor.create();
//     this.editor = editor;
//     this.setState(({isEditor: true}));
//
//     // // callbacks
//     // await game.level.setupCallback('onFinishGoal', this.onFinishGoal);
//     // await game.level.setupCallback('onCompleteGoal', this.onCompleteGoal);
//
//     await game.start(data);
//
//
//     EditorView.game = game;
//     EditorView.editor = editor;
//     console.log('Init game!');
//   };
//   componentWillMount(){
//     //this.editor = new Editor();
//   }
//
//   componentWillUnmount(){
//     this.stop();
//     this.mount.removeChild(this.renderer.domElement)
//   }
//
//   start = () => {
//     if (!this.frameId) {
//       this.frameId = requestAnimationFrame(this.animate)
//     }
//   };
//
//   stop = () => {
//     cancelAnimationFrame(this.frameId)
//   };
//
//   animate = () => {
//     this.renderScene();
//     this.frameId = window.requestAnimationFrame(this.animate)
//   };
//
//   renderScene = () => {
//     this.renderer.render(this.scene, this.camera)
//   };
//
//   onWindowResize = () =>{
//     this.camera.aspect = window.innerWidth / window.innerHeight;
//     this.camera.updateProjectionMatrix();
//     this.renderer.setSize( window.innerWidth, window.innerHeight );
//   };
//
//
//   onMouseDown = (evt) => {
//     let mouse = new THREE.Vector2();
//     mouse.x = ((evt.clientX) / this.renderer.domElement.width) * 2 - 1;
//     mouse.y = -((evt.clientY) / this.renderer.domElement.height) * 2 + 1;
//
//     this.raycaster.setFromCamera(mouse, this.camera);
//   };
//
//   render(){
//     const { isEditor } = this.state;
//     return([
//         <div
//           key={2}
//           style={{ width: '100%', height: '102%',
//             position: 'absolute',
//             backgroundColor: 'black'
//           }}
//           onMouseDown={this.onMouseDown}
//           ref={(mount) => { this.mount = mount }}
//         />,
//         <GuiView
//          editor={this.editor}
//          isEditor={this.state.isEditor}
//         />,
//         <div>{isEditor ? <PreviewView editor={this.editor} /> : null}</div>
//       ]
//     )
//   }
// }
//
// export default EditorView;