import {Component} from "react";
import React from "react";
import * as dg from 'dis-gui';


class GUIView extends Component {
  render() {
    const {
      startGoal,
      onPasteData
    } = this.props;

    return (
      <div className="gui">
        <div className="gui__block">
          <dg.GUI className={'goal__data'}>
            <dg.Text
              onChange={onPasteData}
              label='Data'
              value={''}
            />
            <dg.Button
              label='Start'
              onClick={function() { startGoal() }}
            />


          </dg.GUI>
        </div>
      </div>
    );
  }
}

export default GUIView ;