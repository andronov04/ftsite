import {Component} from "react";
import React from "react";
import * as THREE from "three";
import OrbitControlsClass from "../../lib/OrbitControls";
import Game from "../../Game/Game";
import CanvasRecorder from "../../CanvasRecorder/CanvasRecorder";
import GUIView from "./GUI/GUIView";

let TEST_LEVEL = {
  "data":{
    "backgroundColor":"#1b2759",
    "cameraPosition":[-587.4523452876416, 512.1363808859072, 307.8885864949639],
    "elements": [
      {
        "id": "42F77E5A-EB58-41E2-8F52-5CF0202E0702",
        "type": "line",
        "kind": "linePass",
        "positions": [
          [
            185.00002885618412,
            0,
            130.0000226127628
          ],
          [
            134.99999692809178,
            0,
            150.00000168069664
          ],
          [
            94.9999626969005,
            0,
            162.4999474119678
          ]
        ]
      },
      {
        "id": "AB90E9B4-296A-49C8-9620-07178AF3131A",
        "type": "line",
        "kind": "lineCross",
        "positions": [
          [
            94.9999626969005,
            0,
            162.4999474119678
          ],
          [
            34.99994275620547,
            22.464757828197577,
            79.99988539435446
          ],
          [
            -22.5000862828596,
            42.86309350806809,
            -10.000215718888057
          ]
        ]
      },
      {
        "id": "ECB17FE3-9EFD-473B-804B-5D787D1F82FC",
        "type": "player",
        "positions": [
          [
            185.00002885618412,
            0,
            130.0000226127628
          ]
        ]
      },
      {
        "id": "D7A0ABC5-3F64-4BA4-BA29-5ABC482CBF3C",
        "type": "player",
        "positions": [
          [
            94.9999626969005,
            0,
            162.4999474119678
          ]
        ]
      },
      {
        "id": "8D669F4F-3E14-4C1D-825A-9C13D910D063",
        "type": "player",
        "positions": [
          [
            -25.000048537490876,
            0,
            107.50000566212816
          ]
        ]
      },
      {
        "id": "0783CAB3-2333-462D-B671-F605B7408574",
        "type": "rival",
        "positions": [
          [
            17.4999684421592,
            0,
            120.0000152562369
          ]
        ]
      },
      {
        "id": "385F1080-3D82-4043-98DD-DD6FF18A3A54",
        "type": "rival",
        "positions": [
          [
            157.50001931403025,
            0,
            127.50002048754891
          ]
        ]
      },
      {
        "id": "A35B6443-1AEB-4781-8A49-8D672F2B367E",
        "type": "rival",
        "positions": [
          [
            132.50001000465056,
            0,
            130.00002231275926
          ]
        ]
      },
      {
        "id": "D59FAA0C-B159-49BC-99B3-887DF092CBB9",
        "type": "rival",
        "positions": [
          [
            130.0000066843735,
            0,
            167.50004938766995
          ]
        ]
      },
      {
        "id": "84C88B5A-986B-413A-87FC-B84331C4A461",
        "type": "player",
        "positions": [
          [
            -92.50005130472061,
            0,
            265.00011990676086
          ]
        ]
      },
      {
        "id": "F106FD00-791A-4C6E-884D-DAEA7B1257BA",
        "type": "player",
        "positions": [
          [
            360.0000826374699,
            0,
            190.00006468784795
          ]
        ]
      }
    ],
    "points": [
      {
        "pos": [
          185.00002885618412,
          0,
          130.0000226127628
        ],
        "sort": 0,
        "id": "ed496125-c15f-436d-bf65-51efc03df834",
        "player": "ECB17FE3-9EFD-473B-804B-5D787D1F82FC",
        "ball": 1,
        "rival": null,
        "line": "42F77E5A-EB58-41E2-8F52-5CF0202E0702",
        "durationTime": 2,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          94.9999626969005,
          0,
          162.4999474119678
        ],
        "sort": 2,
        "id": "5235d63f-9f50-4c47-a7ce-7044d18ede17",
        "player": "D7A0ABC5-3F64-4BA4-BA29-5ABC482CBF3C",
        "ball": 1,
        "rival": null,
        "line": "AB90E9B4-296A-49C8-9620-07178AF3131A",
        "durationTime": 1.5,
        "startTime": 1.5,
        "static": true
      },
      {
        "pos": [
          -22.5000862828596,
          42.86309350806809,
          -10.000215718888057
        ],
        "sort": 4,
        "id": "396b9a90-b801-49c1-ad20-04e382259871",
        "player": null,
        "ball": 1,
        "rival": null,
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          -25.000048537490876,
          0,
          107.50000566212816
        ],
        "sort": 5,
        "id": "62457307-1fdd-4c57-8b4c-897a804b7227",
        "player": "8D669F4F-3E14-4C1D-825A-9C13D910D063",
        "ball": null,
        "rival": null,
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          17.4999684421592,
          0,
          120.0000152562369
        ],
        "sort": 6,
        "id": "d4986775-4cd7-4752-b6e1-bc03bfc8c9e5",
        "player": null,
        "ball": null,
        "rival": "0783CAB3-2333-462D-B671-F605B7408574",
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          157.50001931403025,
          0,
          127.50002048754891
        ],
        "sort": 7,
        "id": "1bc222df-dedf-49f0-842a-7b02962a4234",
        "player": null,
        "ball": null,
        "rival": "385F1080-3D82-4043-98DD-DD6FF18A3A54",
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          132.50001000465056,
          0,
          130.00002231275926
        ],
        "sort": 8,
        "id": "065a285d-17c1-439a-934b-e8d34ed87352",
        "player": null,
        "ball": null,
        "rival": "A35B6443-1AEB-4781-8A49-8D672F2B367E",
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          130.0000066843735,
          0,
          167.50004938766995
        ],
        "sort": 9,
        "id": "eed3cbf4-30e1-4275-b0d7-ea88ee159f43",
        "player": null,
        "ball": null,
        "rival": "D59FAA0C-B159-49BC-99B3-887DF092CBB9",
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          -92.50005130472061,
          0,
          265.00011990676086
        ],
        "sort": 10,
        "id": "bac80592-131f-448d-9312-131fbc236356",
        "player": "84C88B5A-986B-413A-87FC-B84331C4A461",
        "ball": null,
        "rival": null,
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          360.0000826374699,
          0,
          190.00006468784795
        ],
        "sort": 11,
        "id": "25236b8f-32ce-4b35-8aa5-401e4f6eacd6",
        "player": "F106FD00-791A-4C6E-884D-DAEA7B1257BA",
        "ball": null,
        "rival": null,
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      }
    ],
    "color": {
      "field": "#fff",
      "ball": "#fff",
      "line": "#fff",
      "player": "#fff",
      "rival": "#fff",
      "point": "#fff"
    }
  },
  "id":"8f746307-4ae6-48b3-8325-74a9ec48701e",
  "tips":[
    {
      "sort":0,
      "tip":"PLACE",
      "title":"London",
      "use":false
    },
    {
      "sort":1,
      "tip":"DATE",
      "title":"28/05/2011",
      "use":false
    },
    {
      "sort":2,
      "tip":"MINUTE",
      "title":"34'",
      "use":false
    }
  ],
  "answers":[
    {
      "title":"Xavi",
      "sort":0,
      "correct":false
    },
    {
      "title":"Messi",
      "sort":1,
      "correct":false
    },
    {
      "title":"Rooney",
      "sort":2,
      "correct":true
    },
    {
      "title":"Lampard",
      "sort":3,
      "correct":false
    }
  ],
  "isAnswered":false,
  "isAsAnswered":null,
  "sort":0,
  "counter":0,
  "colors":[
    "#1b2759",
    "#273981"
  ],
  "shortInfo":"10 Greatest Champions League Final Goals",
  "levelId":5000,
  "fact":"FIFA has more member countries in it than the U.N."
};

class GIFView extends Component {
  raycaster = new THREE.Raycaster();

  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: '#000000',
      level: TEST_LEVEL
    };

  }

  componentDidMount = async() =>{
    const width = this.mount.clientWidth;
    const height = this.mount.clientHeight;
    //ADD SCENE
    this.scene = new THREE.Scene();
    window.scene = this.scene;
    this.camera = new THREE.PerspectiveCamera( 50, width / height, 0.1, 10000 );
    window.camera = this.camera;
    this.camera.position.set(...this.state.level.data.cameraPosition.map(x=> x+100));
    //ADD RENDERER
    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: false });
     this.renderer.setClearColor(this.state.level.data.backgroundColor);
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize(width, height);
    this.mount.appendChild(this.renderer.domElement);
    //await this.setText(this.scene);

    this.controls = new OrbitControlsClass.OrbitControls(this.camera, this.renderer.domElement );
    this.controls.maxPolarAngle = Math.PI / 2 - 0.1;
    //controls.minPolarAngle = 2;
    this.controls.minDistance = 0;
    this.controls.maxDistance = 2000;
    this.controls.rotateSpeed = 0.07;

    // window.addEventListener( 'resize', this.onWindowResize, false );
    //
    //this.start();
    // await this.startLevel(this.scene, this.camera, this.renderer, this.controls, TEST_LEVEL.data);
  };

  startLevel = async (scene, camera, renderer, controls, data) => {


    let game = new Game(scene, camera, renderer, controls, null);
    // await game.initField();
    await game.initGame();


    GIFView.canvasRecorder = new CanvasRecorder(this.renderer.domElement, this.state.level.id);


    // callbacks
    // await game.level.setupCallback('onFinishGoal', this.onFinishGoal);
    await game.level.setupCallback('onCompleteGoal', this.onCompleteGoal);

    //capture
    //await GIFView.canvasRecorder.start();

    GIFView.game = game;
    GIFView.data = data;
    console.log('Init game!');
  };

  startGoal = async () => {
    await GIFView.canvasRecorder.start();

    await GIFView.game.start(GIFView.data);
    // setTimeout(async () => {
    //   await GIFView.game.start(GIFView.data);
    // },1000)
  };

  onCompleteGoal = async() => {
    console.log('onCompleteGoal-onCompleteGoal');
    await GIFView.canvasRecorder.stop();
   // await GIFView.canvasRecorder.save()
    // var colors = {top:"#0ba360", bottom:"#3cba92"};
    //
    // let t0 = new TimelineMax({
    // });  // initial tween timeline animation
    // var tween = t0.to(colors, 3, {top:"#a33193", bottom:"#ba202c", onUpdateParams:[this.mount], onUpdate:colorize});
    //
    // function colorize(element) {
    //   element.style.backgroundImage = "linear-gradient(to top," + colors.top + ", " + colors.bottom + ")";
    // }
    //
    // await GIFView.game.level.onFinishGoal()
  };

  onFinishGoal = async() => {
    console.log('onFinishGoal-onFinishGoal');
    if(!this.flag){
      this.flag = true;
      this.setState({level: null});
      await GIFView.game.start(null)
    }

  };

  onPasteData = async (stringData) => {
    let data = JSON.parse(stringData);
    this.setState({
      level: data
    });

    this.camera.position.set(...data.data.cameraPosition.map(x=> x+100));
    this.renderer.setClearColor(data.data.backgroundColor);
    await this.startLevel(this.scene, this.camera, this.renderer, this.controls, data.data);

  };

  componentWillMount(){
    //this.editor = new Editor();
  }

  componentWillUnmount(){
    this.stop();
    this.mount.removeChild(this.renderer.domElement)
  }

  onWindowResize = () =>{
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  };


  onMouseDown = (evt) => {
    let mouse = new THREE.Vector2();
    mouse.x = ((evt.clientX) / this.renderer.domElement.width) * 2 - 1;
    mouse.y = -((evt.clientY) / this.renderer.domElement.height) * 2 + 1;

    this.raycaster.setFromCamera(mouse, this.camera);
  };

  render(){
    // <BaseView/>
    return([
        <div
          key={1}
          style={{ width: '736px', height: '736px',
            position: 'absolute',
            // backgroundImage: 'linear-gradient(to top, #0ba360 0%, #3cba92 100%)'
          }}
          onMouseDown={this.onMouseDown}
          ref={(mount) => { this.mount = mount }}
        />,
        <GUIView key={2} onPasteData={this.onPasteData} startGoal={this.startGoal}/>
      ]
    )
  }
}

export default GIFView;