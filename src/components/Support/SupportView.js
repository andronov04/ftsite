import {Component} from "react";
import React from "react";
import {Link} from "react-router-dom";


class SupportView extends Component {
  render() {

    return (
      <div style={{ width: '100%', height: '102%',
        overflow: 'auto',
        backgroundImage: 'linear-gradient(to top, #2c9a40 0%, #2c9a40 100%)'
      }}>

        <main>
          <header className="base" style={{position: 'relative'}}>
            <div className="left">
              <Link to={'/'}><div className="name">Who <br/>Scored <br/>The <br/>Goal?</div></Link>
            </div>
          </header>
        </main>

        <article className={'support'}>
          <h1>Get in touch with us</h1>
          <h2>Do you need help in our game or just wanna say 'Hello'?</h2>
          <h3>
            Email us: <a href={"mailto:support@whoscoredthegoal.com"}><b>support@whoscoredthegoal.com</b></a>
          </h3>
        </article>

      </div>
    );
  }
}

export default SupportView;