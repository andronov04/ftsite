import {Component} from "react";
import React from "react";
import AnimationView from "./Animation/AnimationView";


class HomeView extends Component {
  render() {
    return (
      <div className="home">
        <AnimationView />
      </div>
    );
  }
}

export default HomeView ;