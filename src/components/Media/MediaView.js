import {Component} from "react";
import React from "react";
import * as THREE from "three";
import OrbitControlsClass from "../../lib/OrbitControls";
import Game from "../../Game/Game";
import CanvasRecorder from "../../CanvasRecorder/CanvasRecorder";
import GUIView from "../GIF/GUI/GUIView";

let TEST_LEVEL = {
  data: {
    "backgroundColor":"#ed248e",
    "cameraPosition":[
      // 389.3029428582713,
      // 310.24205960331875,
      // 524.966418708877
      // 325.58667395844776,
      // 492.225101939514,
      // 1059.1854920482424
      -309.26041693504686,
      589.7815303691531,
      738.1110852651293
    ],
    "elements": [
      {
        "id": "ECC9414F-34F3-48E2-8AB1-D768615B0F3B",
        "type": "line",
        "kind": "lineCross",
        "positions": [
          [
            -83.49975669765917,
            0,
            279.35229569306387
          ],
          [
            -45.586608419709336,
            42.05250132895011,
            544.1537290118838
          ],
          [
            -66.05775968204696,
            0,
            848.4630080830478
          ]
        ]
      },
      {
        "id": "0935D6E6-01BF-4A09-B6A5-3B74DE86FC12",
        "type": "line",
        "kind": "lineCross",
        "positions": [
          [
            -66.05775968204696,
            0,
            848.4630080830478
          ],
          [
            10.210220260917856,
            94.81061885425113,
            528.4710643692551
          ],
          [
            -18.098790290498442,
            14.67650277596924,
            -9.487563403225522
          ]
        ]
      },
      {
        "id": "8FC8F586-626D-4E7E-A1A3-1F29B17FC08E",
        "type": "rival",
        "positions": [
          [
            -87.11180028157455,
            0,
            281.9992963791817
          ]
        ]
      },
      {
        "id": "04B36DAF-6B4F-4ADF-9117-40D9F3B4467C",
        "type": "player",
        "positions": [
          [
            -68.10973480567674,
            0,
            275.4946611517098
          ]
        ]
      },
      {
        "id": "3B53225C-82DD-4540-AAC9-0CC9C3F495EB",
        "type": "rival",
        "positions": [
          [
            -59.05306875107277,
            0,
            275.30773752987204
          ]
        ]
      },
      {
        "id": "2FEC82B5-9E9B-4A6C-9E6D-3256C4DC5C58",
        "type": "rival",
        "positions": [
          [
            -138.44920182777875,
            0,
            812.8271546727259
          ]
        ]
      },
      {
        "id": "BF76393F-5073-4F7D-93E2-1AD6A40669C9",
        "type": "rival",
        "positions": [
          [
            48.531343097996285,
            0,
            808.8656083468657
          ]
        ]
      }
    ],
    "points": [
      {
        "pos": [
          -83.49975669765917,
          0,
          279.35229569306387
        ],
        "sort": 0,
        "id": "be25aba8-5d37-481c-8e61-63e00ffbe48e",
        "player": null,
        "ball": 1,
        "rival": null,
        "line": "ECC9414F-34F3-48E2-8AB1-D768615B0F3B",
        "durationTime": 4,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          -66.05775968204696,
          0,
          848.4630080830478
        ],
        "sort": 2,
        "id": "c1b46d46-b510-4d1d-9409-d77dde525de7",
        "player": null,
        "ball": 1,
        "rival": null,
        "line": "0935D6E6-01BF-4A09-B6A5-3B74DE86FC12",
        "durationTime": 3,
        "startTime": 4.1,
        "static": true
      },
      {
        "pos": [
          -18.098790290498442,
          14.67650277596924,
          -9.487563403225522
        ],
        "sort": 4,
        "id": "b245bcce-9f83-4f3d-9b1d-fe3f16ce7432",
        "player": null,
        "ball": null,
        "rival": null,
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          -87.11180028157455,
          0,
          281.9992963791817
        ],
        "sort": 5,
        "id": "def0379f-380d-4596-b2a2-a5eaeac16a2d",
        "player": null,
        "ball": null,
        "rival": "8FC8F586-626D-4E7E-A1A3-1F29B17FC08E",
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          -68.10973480567674,
          0,
          275.4946611517098
        ],
        "sort": 6,
        "id": "8776ca2e-80a8-4019-ae3c-f13c7cb5b5b2",
        "player": "04B36DAF-6B4F-4ADF-9117-40D9F3B4467C",
        "ball": null,
        "rival": null,
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          -59.05306875107277,
          0,
          275.30773752987204
        ],
        "sort": 7,
        "id": "e53b4739-134e-4cd5-8c90-f290308461db",
        "player": null,
        "ball": null,
        "rival": "3B53225C-82DD-4540-AAC9-0CC9C3F495EB",
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          -138.44920182777875,
          0,
          812.8271546727259
        ],
        "sort": 8,
        "id": "4989d598-d0ff-46e9-97f4-50d0ee3245b7",
        "player": null,
        "ball": null,
        "rival": "2FEC82B5-9E9B-4A6C-9E6D-3256C4DC5C58",
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      },
      {
        "pos": [
          48.531343097996285,
          0,
          808.8656083468657
        ],
        "sort": 9,
        "id": "a363c0d4-72ce-49b5-b3e0-dfda101fbc43",
        "player": null,
        "ball": null,
        "rival": "BF76393F-5073-4F7D-93E2-1AD6A40669C9",
        "line": null,
        "durationTime": 5,
        "startTime": 0,
        "static": true
      }
    ]
  },
  "id":0,
  "tips":[
    {
      "sort":0,
      "tip":"PLACE",
      "title":"Milan",
      "use":false
    },
    {
      "sort":1,
      "tip":"DATE",
      "title":"06/10/2012",
      "use":false
    },
    {
      "sort":2,
      "tip":"MINUTE",
      "title":"67'",
      "use":false
    }
  ],
  "answers":[
    {
      "title":"Carlos",
      "sort":0,
      "correct":false
    },
    {
      "title":"Henry",
      "sort":1,
      "correct":false
    },
    {
      "title":"Zidane",
      "sort":2,
      "correct":true
    },
    {
      "title":"Beckham",
      "sort":3,
      "correct":false
    }
  ],
  "isAnswered":false,
  "isAsAnswered":null,
  "sort":0,
  "shortInfo":"Europe League",
  "levelId":1
};

class MediaView extends Component {
  raycaster = new THREE.Raycaster();

  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: '#000000',
      level: TEST_LEVEL
    };

  }

  componentDidMount = async() =>{
    const width = this.mount.clientWidth;
    const height = this.mount.clientHeight;
    //ADD SCENE
    this.scene = new THREE.Scene();
    window.scene = this.scene;
    this.camera = new THREE.PerspectiveCamera( 50, width / height, 0.1, 10000 );
    window.camera = this.camera;
    this.camera.position.set(...this.state.level.data.cameraPosition.map(x=> x+100));
    //ADD RENDERER
    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: false });
    this.renderer.setClearColor('#ed248e');
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize(width, height);
    this.mount.appendChild(this.renderer.domElement);
    //await this.setText(this.scene);

    this.controls = new OrbitControlsClass.OrbitControls(this.camera, this.renderer.domElement );
    this.controls.maxPolarAngle = Math.PI / 2 - 0.1;
    //controls.minPolarAngle = 2;
    this.controls.minDistance = 0;
    this.controls.maxDistance = 2000;
    this.controls.rotateSpeed = 0.07;

    window.addEventListener( 'resize', this.onWindowResize, false );

    //this.start();
    await this.startLevel(this.scene, this.camera, this.renderer, this.controls, TEST_LEVEL.data);
  };

  startLevel = async (scene, camera, renderer, controls, data) => {


    let game = new Game(scene, camera, renderer, controls, null);
    // await game.initField();
    await game.initGame();


    MediaView.canvasRecorder = new CanvasRecorder(this.renderer.domElement);


    // callbacks
    // await game.level.setupCallback('onFinishGoal', this.onFinishGoal);
    await game.level.setupCallback('onCompleteGoal', this.onCompleteGoal);

    //capture
    //await MediaView.canvasRecorder.start();

    MediaView.game = game;
    MediaView.data = data;
    console.log('Init game!');
  };

  startGoal = async () => {
    await MediaView.canvasRecorder.start();

    await MediaView.game.start(MediaView.data);
  };

  onCompleteGoal = async() => {
    console.log('onCompleteGoal-onCompleteGoal');
    await MediaView.canvasRecorder.stop();
    // await MediaView.canvasRecorder.save()
    // var colors = {top:"#0ba360", bottom:"#3cba92"};
    //
    // let t0 = new TimelineMax({
    // });  // initial tween timeline animation
    // var tween = t0.to(colors, 3, {top:"#a33193", bottom:"#ba202c", onUpdateParams:[this.mount], onUpdate:colorize});
    //
    // function colorize(element) {
    //   element.style.backgroundImage = "linear-gradient(to top," + colors.top + ", " + colors.bottom + ")";
    // }
    //
    // await MediaView.game.level.onFinishGoal()
  };

  onFinishGoal = async() => {
    console.log('onFinishGoal-onFinishGoal');
    if(!this.flag){
      this.flag = true;
      this.setState({level: null});
      await MediaView.game.start(null)
    }

  };

  componentWillMount(){
    //this.editor = new Editor();
  }

  componentWillUnmount(){
    this.stop();
    this.mount.removeChild(this.renderer.domElement)
  }

  onWindowResize = () =>{
    // this.camera.aspect = window.innerWidth / window.innerHeight;
    // this.camera.updateProjectionMatrix();
    // this.renderer.setSize( window.innerWidth, window.innerHeight );
  };


  onMouseDown = (evt) => {
    let mouse = new THREE.Vector2();
    mouse.x = ((evt.clientX) / this.renderer.domElement.width) * 2 - 1;
    mouse.y = -((evt.clientY) / this.renderer.domElement.height) * 2 + 1;

    this.raycaster.setFromCamera(mouse, this.camera);
  };

  render(){
    // <BaseView/>
    return([
        <div
          key={1}
          style={{ width: '455px', height: '850px',
            position: 'absolute',
            // backgroundImage: 'linear-gradient(to top, #0ba360 0%, #3cba92 100%)'
          }}
          onMouseDown={this.onMouseDown}
          ref={(mount) => { this.mount = mount }}
        />,
        <GUIView key={2} startGoal={this.startGoal}/>
      ]
    )
  }
}

export default MediaView;