import {Component} from "react";
import React from "react";
import {Link} from "react-router-dom";


class PrivacyPolicy extends Component {
  render() {

    return (
      <div style={{ width: '100%', height: '102%',
        overflow: 'auto',
        backgroundImage: 'linear-gradient(to top, #2c9a40 0%, #2c9a40 100%)'
      }}>

        <main>
          <header className="base" style={{position: 'relative'}}>
            <div className="left">
              <Link to={'/'}><div className="name">Who <br/>Scored <br/>The <br/>Goal?</div></Link>
            </div>
          </header>
        </main>

        <article className="privacy">
          <h1>Privacy Policy</h1>
          This Privacy Policy applies to the game "Who scored the goal?" on mobile devices.

          By downloading or using our applications or interacting with our websites, you agree to the collection and use of your information in accordance with this Privacy Policy. If you do not wish us to collect and use your data in the way described in this Privacy Policy, you should stop using our applications and websites.

          <h1>What Information We Collect</h1>

          If you contact us, we may collect and process the information that you freely provide to us. This information includes the content of your e-mails, messages and comments, responses to messages that we send to you, your interactions with us on social media websites and third party platforms. The collected information may include your e-mail addresses, user names or other personal information if you decided to provide it when you contacted us.

          We use AdSense (AdMob) advertising service in our applications. Please refer also to https://www.google.com/policies/technologies/partner-sites/ for additional information.

          <h1>How We Use Collected Information</h1>

          We use the information we collect about you for providing our applications and other services to you, optimizing our applications and your user experience, storing your progress through our applications, providing customer service support, contacting you as part of customer service or to send you updates about our applications and services.

          We may also use your information for research, analysis, business planning, tracking potential problems or trends with our applications, testing out new application features and content.

          <h1>Third Party Advertisers</h1>

          We show third party advertisements in our applications. Our advertising partners may collect information about you when you use our applications and use this information for the targeted advertising. Advertisers on mobile devices may use advertising identifiers, such as Apple’s ID for advertising (IDFA) or the Android advertising ID, to enable and optimize their advertising. Advertising identifiers are non-permanent, non-personal device identifiers. You can reset these identifiers or indicate your preference that they should not be used for advertising purposes by changing the settings of your device.

          Our applications and websites may contain links to third party websites, services, and/or applications (including advertising that may link to a third party). We are not responsible for the privacy practices, safety, and the content of these websites, services, and/or applications.

          <h1>Privacy Policy Updates</h1>

          We may occasionally update this Privacy Policy. This version of the Privacy Policy was last updated on 24 April 2019.

          <h1>Contact</h1>

          If you have questions regarding this Privacy Policy, please contact us via e-mail at support@whoscoredthegoal.com.

        </article>

      </div>
    );
  }
}

export default PrivacyPolicy;