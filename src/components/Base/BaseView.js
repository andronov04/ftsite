import {Component} from "react";
import React from "react";
import {Link} from "react-router-dom";


class BaseView extends Component {
  render() {
    const { level } = this.props;
    console.log(level);

    return (
      <div >

        <main>
          <header className="base">
            <div className="left">
              <div className="name">Who <br/>Scored <br/>The <br/>Goal?</div>
              <div className="tournament">{level.shortInfo.toUpperCase()}</div>
            </div>
            <div className="right">
              <div className="app">
                <span className="app-o">Get in on</span>
                <a className="app_link" href={"https://play.google.com/store/apps/details?id=com.whoscoredthegoal.whoscoredthegoal"}>
                   Google Play
                </a>
                <br/>
                <span className="app-o">Download on the</span>
                <a className="app_link" href={"https://itunes.apple.com/us/app/who-scored-the-goal/id1458414037"}>
                   App Store
                </a>
              </div>
              <div className="data">
                {level.tips.reverse().map(tip=>
                 <span style={{marginLeft: '10px'}}>{tip.title.toUpperCase()}</span>
                )}
              </div>
            </div>
          </header>
        </main>

        <footer className="base">
          <div className="description">
            <div className="slogan">Guess the goal on the ball trajectory</div>
            <div className="text">For true fans of football, we have created a quiz.
              Check your knowledge, share with friends and learn interesting facts that you did not know.
            </div>
          </div>
          <div className="info">
            <div><Link to={"/privacy-policy"}>Privacy Policy</Link></div>
            <div style={{paddingLeft: '15px'}}><Link to={"/support"}>Support</Link></div>
            {/*<div style={{paddingLeft: '15px'}}><a href={"mailto:support@whoscoredthegoal.com"}>Support</a></div>*/}
          </div>
        </footer>

      </div>
    );
  }
}

export default BaseView ;