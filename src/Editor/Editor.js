import Line from "../Game/Line";
import EditorPoint from "./EditorPoint";
import uuid from "uuid";
import DragControlsClass from "../lib/DragControls";
import TransformControlsClass from "../lib/TransformControls";
import ElementFactory from "../Game/Element";
import * as THREE from "three";
import Point from "../Game/Point";


const DEFAULT_POINT_POSITION = [100, 0, 100];
const DEFAULT_KIND_LINE = "lineCross";
const DEFAULT_DURATION_TIME = 5;
const TYPE_LINE = 'line';
const TYPE_PLAYER = 'player';
const TYPE_RIVAL = 'rival';
const ID_BALL = 1;



let cl  = { // TODO test
  field: "#fff",
  ball: "#fff",
  line: "#fff",
  player: "#fff",
  rival: "#fff",
  point: "#fff"
};

class Editor {
  constructor(scene, camera, controls, renderer, raycaster) {
    this.scene = scene;
    this.camera = camera;
    this.controls = controls;
    this.renderer = renderer;
    this.raycaster = raycaster;
    this.dragcontrols = null;

    this._3dView = false;

    this._points = []; // for json
    this._dragElements = []; // for drag controls
    this._elements = [];

    this._setState = null;

    this._preview = {
      scene: null,
      camera: null,
      renderer: null,
      controls: null,
      game: null
    }
  }

  create = async() => {
    let center = this.scene.children.find(a=>a.userData.field && a.userData.field === 'pointCenter');
    this.controls.target = center.position;

    await this.initDragAndDrop();
  };

  setupPreview = async(scene, camera, renderer, controls, game) => {
    this._preview = {
      scene,
      camera,
      renderer,
      controls,
      game
    }
  };

  switchView = async() => {
    this._3dView = !this._3dView;
    console.log('switchView', this._3dView, !this._3dView);

    this.camera.position.set(
      this._3dView ? 371 : 0,
      this._3dView ? 319 : 877,
      this._3dView ? 880 : 0
    );
    this.controls.maxPolarAngle = this._3dView ? Math.PI*2 : THREE.Math.degToRad(-180);
    this.controls.maxAzimuthAngle = this._3dView ? Math.PI*2 : THREE.Math.degToRad(-90);

    this.transformControl.showY = this._3dView;
    this.transformControl.showX = !this._3dView;
    this.transformControl.showZ = !this._3dView;



  };

  addPoint = async () => {

    let point = {
      "positions":[DEFAULT_POINT_POSITION],
      "sort":0,
      "id":uuid.v4(),
      "player":null,
      "ball":null,
      "rival":null,
      "line":null,
      "durationTime":1,
      "startTime":0,
      "static":true
    };

    let elem = await new Point(point).create();
    this.scene.add(elem);
    this.callDragElements(elem);

    return point;
  };

  toLevel = async (isSave=false) => { //TODO async
    console.log('POINTS:', this._points);
    console.log('ELEMENTS', this._elements);

    let animElements = this._elements.map(item=>{
      return {
        id: item.uuid,
        type: item.userData.type,
        kind: item.userData.kind,
        positions: item.userData.type === TYPE_LINE ? Object.values(item.userData.points) : [item.position.toArray()]
      }
    });
    console.log('ANIM ELEMENTS: ', animElements);

    let animPoints = this._points.filter(a=>a.isActive).map(item=>{
      return {
        pos: item.pos.toArray(),
        sort: item.sort,
        id: item.id,

        player: item.player ? item.player.uuid : null,
        ball: item.ball,
        rival: item.rival ? item.rival.uuid : null,
        line: item.line ? item.line.uuid : null,

        durationTime: item.durationTime,
        startTime: item.startTime,
        static: item.static,
      }
    });

    console.log('ANIM POINTS: ', [...animPoints]);

    let data = {
      id: uuid.v4(),
      elements: animElements,
      points: animPoints
    };

    if(isSave){
      let jsonDATA = JSON.stringify(data,null,'\t');
      navigator.clipboard.writeText(data.id);

      await this.download(jsonDATA, `${data.id}.json`, 'text/plain');
    }

    // remove elements
    await this._preview.game.level.removeElements().then(async () => {
      // restart level
      await this._preview.game.level.start(data);
    });

    return data
  };

  levelToEditor = (level) => {
    window.GOAL = level;
    console.log('levelToEditor', level, this.scene);

    //new EditorPoint(_item).create();
    // level.data.elements.forEach(item=>{
    //
    //   if(item.positions) {
    //     console.log('SSSSS', item.positions[1])
    //     let _item = {
    //       type: 'point',
    //       positions: [item.positions[1]],
    //     };
    //     let a = ]new EditorPoint(_item).create(_item);
    //     this.scene.add(a)
    //   }
    //   // let it = this.scene.children.find(a=>a.userData && a.userData.id === item.id);
    //   // // console.log('itttt', it)
    //   // this.callDragElements(it)
    // });

    this.scene.children.filter(a=>a.userData && a.userData.type).forEach(item=> {
      this.callDragElements(item)
    });

    //
    // level.data.points = level.data.points.map(item=>{
    //   console.log('item', item);
    //   return this.addPoint(item.positions, true, item)
    //   // return this.callPoints(item);
    //
    //   // this.callDragElements(item);
    //   // this.callPoints(item, true);
    // });
    //
    // console.log(this.scene);

    return level
  };

  changePlayer = async(point, isAdd=true) => {
    console.log('changePlayer', point, isAdd);

    if(!isAdd){
      await this.callElements(point.player, false);
      point.player = null;
      await this.callPoints();
      return
    }

    let item = {
      type: 'player',
      positions: [point.instance.position.toArray()],
    };
    let elem = await ElementFactory.create(item, cl);
    elem.data.color = cl;
    point.instance.userData['element'] = elem;
    elem.userData['type'] = TYPE_PLAYER;

    point.player = elem;

    await this.callElements(elem);
    this.scene.add(elem);
  };

  changeBall = async(point) => {
    console.log('changeBall', point);
    point.ball = point.ball ? null : ID_BALL;
    await this.callPoints();
  };

  changeRival = async(point, isAdd=true) => {
    console.log('changeRival', point);

    if(!isAdd){
      await this.callElements(point.rival, false);
      point.rival = null;
      await this.callPoints();
      return
    }

    let item = {
      type: 'rival',
      positions: [point.instance.position.toArray()],
    };
    let elem = await ElementFactory.create(item, cl);
    elem.data.color = cl;
    point.instance.userData['element'] = elem;
    elem.userData['type'] = TYPE_RIVAL;

    point.rival = elem;

    await this.callElements(elem);
    this.scene.add(elem);
  };

  changeStatic = async(point, isAdd=true) => {
    point.static = isAdd;
    await this.callPoints();
  };

  changeDurationTime = async(point, duration) => {
    point.durationTime = duration;
    await this.callPoints();
  };

  changeStartTime = async(point, timing) => {
    point.startTime = timing;
    await this.callPoints();
  };

  changeKindLine = async(point, kind) => {
    let line = point.line;
    line.material = await line.getMaterialLine(kind);
    line.userData['kind'] = kind;

    await this.callPoints();
  };

  addLine = async(point) => {
    let point0 = point.instance;
    let posX = point0.position.x + 50;
    let posZ = point0.position.z + 50;
    let point1 = await this.addPoint([posX, 0, posZ], false);
    let point2 = await this.addPoint([posX+50, 0, posZ+50]);

    let pos = [
      point0.position.toArray(),
      point1.position.toArray(),
      point2.position.toArray(),
    ];
    let element = await new Line({
      positions: pos,
      kind: DEFAULT_KIND_LINE,
      drawCount: 1000
    },
      cl
      ).create();

    let p = {};
    p[point0.uuid] = pos[0];
    p[point1.uuid] = pos[1];
    p[point2.uuid] = pos[2];

    element.userData['points'] =  p;

    element.userData['type'] = TYPE_LINE;
    element.userData['kind'] = DEFAULT_KIND_LINE;

    //update point for animation
    point.line = element;
    await this.callPoints();

    await this.callElements(element);
    this.scene.add(element)
  };

  callPoints = (point=null, isAdd=true) => {
    if(point && isAdd)
      this._points.push(point);
    // await this._setState(this._points)
  };

  callDragElements = (elem=null, isAdd=true) => {
    if(elem && isAdd)
      this._dragElements.push(elem);
    this.dragcontrols.setObjects(this._dragElements)
  };

  callElements = async(elem=null, isAdd=true) => {
    if(elem && isAdd)
      this._elements.push(elem);

    if(elem && !isAdd)
      this._elements = this._elements.filter(a=>a.uuid !== elem.uuid);
      this.scene.remove(elem)
  };

  updateTransformPoint = async(point) => {
    // recreate line logic
    let lines = this.scene.children.filter(a=>a.userData.points && Object.keys(a.userData.points).includes(point.uuid));
    if(lines.length){
      lines.forEach(async line => {
        let position = line.userData['points'];
        position[point.uuid] = point.position.toArray();
        line.geometry = await line.creatGeometry(Object.values(position));
        line.curve = line.geometry.curve;
        line.computeLineDistances();
      })
    }
    if(point.userData.element){
      point.userData.element.position.set(point.position.x, point.position.y, point.position.z)
    }
  };

  initDragAndDrop = async () => {
    this.dragcontrols = new DragControlsClass.DragControls(this._dragElements, this.camera, this.renderer.domElement ); //
    this.dragcontrols.enabled = false;

    this.transformControl = new TransformControlsClass.TransformControls(this.camera,this.renderer.domElement);
    this.transformControl.damping = 0.2;
    this.transformControl.showY = false;
    // console.log(this.transformControl);

    let self = this;
    let hiding = null;


    function delayHideTransform() {
      cancelHideTransorm();
      hideTransform();
    }
    function hideTransform() {
      hiding = setTimeout(function () {
        self.transformControl.detach(self.transformControl.object);
      }, 2500 );
    }
    function cancelHideTransorm() {
      if (hiding) clearTimeout(hiding);
    }

    this.controls.addEventListener( 'start', function () {
      cancelHideTransorm();
    });
    this.controls.addEventListener( 'end', function () {
      delayHideTransform();
    });

    this.dragcontrols.addEventListener('hoveron', function (event) {
      cancelHideTransorm();
      self.transformControl.attach(event.object);
    });

    // Hiding transform situation is a little in a mess :()
    this.transformControl.addEventListener('change', function (obj) {
      cancelHideTransorm();
    });
    this.transformControl.addEventListener('mouseDown', function () {
      cancelHideTransorm();
    });
    this.transformControl.addEventListener('mouseUp', function () {
      delayHideTransform();
    });
    this.transformControl.addEventListener('objectChange', async function (event) {
      // console.log('objectChange', event.target.object);
      await self.updateTransformPoint(event.target.object)
    });

    this.scene.add(this.transformControl);
  };

  download = async(content, fileName, contentType) => {
    let a = document.createElement("a");
    let file = new Blob([content], {type: contentType});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
  }
}

export default Editor