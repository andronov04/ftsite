import Point from "../Game/Point";
import * as THREE from "three";


class EditorPoint extends Point {
  createMaterial = async ()=> {
    let color = await this.randomColor();
    let material =  new THREE.MeshBasicMaterial({color: color});
    material.hexColor = color;
    return material
  };

  createGeometry = async ()=> {
    return new THREE.SphereGeometry(6, 32, 32);
  };

  randomColor = async ()=>{
    return '#' + ((1<<24)*(Math.random()+1)|0).toString(16).substr(1)
  }

}

export default EditorPoint