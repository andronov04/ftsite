const webApiUrl = "http://localhost:8000/api/v1";

class GameService {

  get = async (collection, urlParams) => {
    const options = {
      method: "GET",
    };
    const request = new Request(`${webApiUrl}/${collection}/?${urlParams}`, options);
    const response = await fetch(request);
    return response.json();
  };

  post = async (collection, data) => {
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    const options = {
      method: "POST",
      headers,
      body: data
    };
    const request = new Request(`${webApiUrl}/${collection}/`, options);
    const response = await fetch(request);
    return response.json();
  };

  put = async (model) => {
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    const options = {
      method: "PUT",
      headers,
      body: JSON.stringify(model)
    };
    const request = new Request(webApiUrl, options);
    return await fetch(request);
  };

  delete = async (id) => {
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    const options = {
      method: "DELETE",
      headers
    };
    const request = new Request(webApiUrl + "/" + id, options);
    return await fetch(request);
  }
}

export default GameService;