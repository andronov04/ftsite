import * as THREE from "three";

class Point {
  constructor(data, color) {
    this.data = data;
    this.color = color;
  }

  create = async () => {
    let pos = this.data.positions[0];

    // create center circle
    let c_geometry =  await this.createGeometry();
    let c_material =  await this.createMaterial();
    let sphere = new THREE.Mesh(c_geometry, c_material);
    sphere.position.set(...pos);
    sphere.scale.set(1, 0.1, 1);
    sphere.userData.type = 'point';

    return sphere
  };

  createGeometry = async ()=> {
    return new THREE.SphereGeometry(2, 32, 32);
  };

  createMaterial = async ()=> {
    return new THREE.MeshBasicMaterial({
      color: this.color && this.color.point || 0xffffff,  //await this.randomColor(),//
      transparent: false
    })
  };

  randomColor = async ()=>{
    return '#' + ((1<<24)*(Math.random()+1)|0).toString(16).substr(1)
  }

}

export default Point