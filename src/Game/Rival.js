import * as THREE from "three";

class Rival {
  constructor(data, color) {
    this.data = data;
    this.color = color;
  }

  create = async() => {
    let path = new THREE.Path();
    path.lineTo( 5, 5 );
    path.lineTo( 10, 10 );
    path.lineTo( 5, 5 );
    path.lineTo( 0, 10 );
    path.lineTo( 10, 0 );

    let points = path.getPoints();
    let geometry = new THREE.BufferGeometry().setFromPoints( points );
    let material = new THREE.LineBasicMaterial( {
      color: this.color && this.color.rival || 0xffffff,
      transparent: true
    } );
    let rival = new THREE.Line( geometry, material );
    let pos = this.data.positions[0];

    rival.position.set(pos[0], pos[1], pos[2]);
    rival.rotateX(-Math.PI/2);
    rival.userData.id = this.data.id;
    rival.userData.type = 'rival';
    return rival
  };

}

export default Rival