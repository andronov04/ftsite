import * as THREE from "three";
import {LENGTH_CURVE_POINTS} from "./constants";

const lineVertShader = `
  attribute float lineDistance;
  varying float vLineDistance;
  
  void main() {
    vLineDistance = lineDistance;
    vec4 mvPosition = modelViewMatrix * vec4( position, 1 );
    gl_Position = projectionMatrix * mvPosition;
  }
  `;

const lineFragShader = `
  uniform vec3 diffuse;
  uniform float opacity;
  uniform float time; // added time uniform

  uniform float dashSize;
  uniform float gapSize;
  uniform float dotSize;
  varying float vLineDistance;
  
  void main() {
		float totalSize = dashSize + gapSize;
		float modulo = mod( vLineDistance + time, totalSize ); // time added to vLineDistance
    float dotDistance = dashSize + (gapSize * .5) - (dotSize * .5);
    
    if ( modulo > dashSize && mod(modulo, dotDistance) > dotSize ) {
      discard;
    }

    gl_FragColor = vec4( diffuse, opacity );
  }
  `;


class Line {
  constructor(data, color) {
    this.data = data;
    this.color = color;
  }

  create = async() => {
    let positions = this.data.positions;

    let geometrySpline = await this.creatGeometry(positions);

    let material = await this.getMaterialLine(this.data.kind, this.color.line);

    let line = new THREE.Line(geometrySpline, material);
    line.computeLineDistances();
    line.curve = geometrySpline.curve;
    line.geometry.setDrawRange(0, this.data.drawCount || 0);
    line.userData.id = this.data.id;
    line.userData.type = this.data.type;
    line.userData.kind = this.data.kind;

    line.creatGeometry = this.creatGeometry;
    line.getMaterialLine = this.getMaterialLine;

    return line
  };

  creatGeometry = async(positions) => {
    let curve = new THREE.QuadraticBezierCurve3(...positions.map(it=>new THREE.Vector3(...it)));

    let samples = curve.getPoints(LENGTH_CURVE_POINTS);

    let geometrySpline = new THREE.BufferGeometry().setFromPoints(samples);
    geometrySpline.curve = curve;
    return geometrySpline
  };

  getMaterialLine = async(kind, color="#ffffff")=>{
    let dct = {
      linePass:  new THREE.LineBasicMaterial({ color: color, linewidth: 1, transparent: true}),
      lineRunWithBall:  new THREE.LineBasicMaterial({ color: color, linewidth: 1, transparent: true}),
      lineEmpty: new THREE.LineBasicMaterial({ opacity: 0, transparent: true}),//transparent: true,
      lineCross: new THREE.LineDashedMaterial({
        color: color,
        linewidth: 1,
        dashSize: 6,
        gapSize: 6, transparent: true
      }),
      lineRun: new THREE.ShaderMaterial({
        uniforms: {
          diffuse: {value: new THREE.Color(color)}, //new THREE.Color(0xff0000)
          dashSize: {value: 10},
          gapSize: {value: 10},
          dotSize: {value: 2},
          opacity: {value: 1.0},
          time: {value: 0} // added uniform
        },
        linewidth: 1,
        vertexShader: lineVertShader,
        fragmentShader: lineFragShader,
        transparent: true
      })
    };
    return dct[kind]
  };
}

export default Line