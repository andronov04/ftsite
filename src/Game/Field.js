import * as THREE from "three";

class Field {

  lines = [];

  create = async(scene, another=false, color='#ffffff') => {
    // poles
    let poleGeo = new THREE.BoxBufferGeometry( 3, 54, 3 );
    let poleMat = new THREE.MeshBasicMaterial({color: color});
    let r_mesh = new THREE.Mesh( poleGeo, poleMat );
    r_mesh.position.set(-75, 27, 0);
    let r_mesh2 = r_mesh.clone();
    r_mesh2.position.set(-75, 27, another ? -1755 : 1755);
    scene.add(r_mesh);
    scene.add(r_mesh2);

    this.lines.push(r_mesh);
    this.lines.push(r_mesh2);

    let l_mesh = new THREE.Mesh( poleGeo, poleMat );
    l_mesh.position.set(75, 27, 0);
    let l_mesh2 = l_mesh.clone();
    l_mesh2.position.set(75, 27, another ? -1755 : 1755);
    scene.add(l_mesh);
    scene.add(l_mesh2);

    this.lines.push(l_mesh);
    this.lines.push(l_mesh2);

    let u_mesh = new THREE.Mesh( new THREE.BoxBufferGeometry( 152, 3, 3 ), poleMat );
    u_mesh.position.set(0, 53, 0);
    let u_mesh2 = u_mesh.clone();
    u_mesh2.position.set(0, 53, another ? -1755 : 1755);
    scene.add(u_mesh);
    scene.add(u_mesh2);

    this.lines.push(u_mesh);
    this.lines.push(u_mesh2);

    let path = new THREE.Path();

    // biggest
    path.lineTo(-750, 0);
    path.lineTo(750, 0);
    path.lineTo(750, 1755);
    path.lineTo(-750, 1755);
    path.lineTo(-750, 0);

    // bigger
    path.lineTo(330, 0);
    path.lineTo(330, 228);
    path.lineTo(-330, 228);
    path.lineTo(-330, 0);
    // big
    path.lineTo(150, 0);
    path.lineTo(150, 75);
    path.lineTo(-150, 75);
    path.lineTo(-150, 0);

    // bigger another
    path.lineTo(-750, 0);
    path.lineTo(-750, 1755);
    path.lineTo(-330, 1755);
    path.lineTo(330, 1755);
    path.lineTo(330, 1527);
    path.lineTo(-330, 1527);
    path.lineTo(-330, 1755);
    // big
    path.lineTo(150, 1755);
    path.lineTo(150, 1680);
    path.lineTo(-150, 1680);
    path.lineTo(-150, 1755);


    // center line
    path.lineTo(-750, 1755);
    path.lineTo(-750, 877);
    path.lineTo(750, 877);

    let points = path.getPoints();
    let geometry = new THREE.BufferGeometry().setFromPoints(points);
    let material = new THREE.LineBasicMaterial({color: color, linewidth: 2});
    let line = new THREE.Line( geometry, material );
    line.rotateX(another ? -Math.PI/2 : Math.PI/2); //change storane prosto minus
    scene.add(line);

    this.lines.push(line);

    //circle center
    let arcShape = new THREE.Shape();
    arcShape.moveTo( 0, 0);
    arcShape.absarc( 0, 0, 100, 0, Math.PI * 2, false );
    points = arcShape.getPoints(100);
    geometry = new THREE.BufferGeometry().setFromPoints(points);
    material = new THREE.LineBasicMaterial({color: color, linewidth: 2});
    let circle = new THREE.Line( geometry, material );
    circle.rotateX(Math.PI/2);
    circle.position.set(0, 0, another ? -877 : 877);
    circle.userData['field'] = 'center';
    scene.add(circle);

    this.lines.push(circle);

    //point center
    let c_geometry = new THREE.SphereGeometry(2, 32, 32);
    let c_material = new THREE.MeshBasicMaterial({color: color, transparent: true});
    let sphere = new THREE.Mesh(c_geometry, c_material);
    sphere.position.set(0, 0, 1755/2);
    sphere.userData['field'] = 'pointCenter';
    sphere.scale.set(1.5, 0.1, 1.5);
    scene.add(sphere);

    this.lines.push(sphere);

    //penalty
    geometry = new THREE.CircleGeometry( 2, 180 );
    material = new THREE.MeshBasicMaterial( { color: color } );
    circle = new THREE.Mesh(geometry, material);
    circle.rotateX(-Math.PI/2);
    circle.scale.set(1.5, 1.5, 1.5);
    circle.position.set(0, 0, another ? -150 : 150);
    let circle2 = circle.clone();
    circle2.position.set(0, 0, another ? -1600 : 1600);
    scene.add(circle);
    scene.add(circle2);

    this.lines.push(circle);
    this.lines.push(circle2);

    arcShape = new THREE.QuadraticBezierCurve3(
      new THREE.Vector3( 105, 0, 228),
      new THREE.Vector3(0, 0, 320),
      new THREE.Vector3( -105, 0, 228),
    );
    points = arcShape.getPoints( 100 );
    geometry = new THREE.BufferGeometry().setFromPoints( points );
    line = new THREE.Line(geometry, new THREE.LineBasicMaterial( { color: color, linewidth: 2 } ) );
    let line2 = line.clone();
    line2.position.set(0, 0, 1755);
    line2.rotateY(-Math.PI);
    scene.add(line);
    scene.add(line2);

    this.lines.push(line);
    this.lines.push(line2);
  };

  changeColor = async(color='#fff')=>{
    this.lines.forEach(item=>{
      item.material = item.type === 'Mesh' ?
        new THREE.MeshBasicMaterial( { color: new THREE.Color(color) } )
        :
        new THREE.LineBasicMaterial( { color: new THREE.Color(color), linewidth: 2 } );
    })
  }

}

export default Field