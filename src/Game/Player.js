import * as THREE from "three";

class Player {
  constructor(data, color) {
    this.data = data;
    this.color = color;
  }

  create = async() => {
    let geometry = this.createGeometry(Math.PI*2, Math.PI*2);
    let material = new THREE.MeshBasicMaterial( {
      color: this.color && this.color.player || 0xffffff,
      side: THREE.DoubleSide,
      transparent: true
    } );
    let mesh = new THREE.Mesh( geometry, material );

    let pos = this.data.positions[0];
    mesh.position.set(...pos);
    mesh.rotateX(-Math.PI/2);
    mesh.userData.id = this.data.id;
    mesh.userData.type = 'player';

    mesh.createGeometry = this.createGeometry;

    // create center circle
    let c_geometry = new THREE.SphereGeometry(2.8, 32, 32);
    let c_material = new THREE.MeshBasicMaterial({
      color: this.color && this.color.player || 0xffffff,
      transparent: true
    });
    let sphere = new THREE.Mesh(c_geometry, c_material);
    sphere.position.set(...pos);
    sphere.scale.set(1, 0.1, 1);
    sphere.userData.id = `s${this.data.id}`;
    sphere.userData.type = 'splayer';
    mesh.sibling = sphere;

    return mesh
  };

  createGeometry = (...args) =>{
    return new THREE.RingGeometry( 8.5, 9, 64, 0, ...args )
  }

}

export default Player