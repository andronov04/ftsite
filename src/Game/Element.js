import Line from "./Line";
import Rival from "./Rival";
import Player from "./Player";
import Ball from "./Ball";
import Point from "./Point";


class ElementFactory {

  static create =  async(data, color) =>{
    if (data.type === 'line')
      return await new Line(data, color).create();
    if (data.type === 'rival')
      return await new Rival(data, color).create();
    if (data.type === 'player')
      return await new Player(data, color).create();
    if (data.type === 'ball')
      return await new Ball(data, color).create();
    if (data.type === 'point')
      return await new Point(data, color).create();
  }
}

export default ElementFactory;