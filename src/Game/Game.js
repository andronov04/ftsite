import {TimelineMax} from "gsap/TweenMax";
import ElementFactory from "./Element";
import Field from "./Field";
import {ANIMATION_REPEAT_TIMEOUT, TYPE_LINE, KIND_LINE_RUN} from "./constants";

const ID_BALL = 1;


class Game {
  constructor(scene, camera, renderer, controls, gl=null) {
    this.scene = scene;
    this.camera = camera;
    this.renderer = renderer;
    this.controls = controls;
    this.gl = gl;
    this.field = new Field();
  }

  render = async() => {
    this.renderer.render(this.scene, this.camera);
    if (this.gl)
      this.gl.endFrameEXP();
  };

  animate = async() =>{
    requestAnimationFrame(this.animate.bind(this));
    await this.render();
  };

  initGame = async(fieldColor='#ffffff')=>{
    await this.field.create(this.scene, false, fieldColor);
    await this.animate();
    this.level = new Level(this.scene, this.camera, this.controls);
  };

  start = async(data) => {
    // await this.field.changeColor(data.color.field); //todo check
    await this.level.start(data);
  }
}

class Level {
  constructor(scene, camera, controls) {
    this.data = null;
    this._elements = [];
    this.scene = scene;
    this.camera = camera;
    this.controls = controls;
    this._isUseControl = false;
    this._timeline = null;
    this.callbacks = {
      onCompleteGoal: async () => {
        console.log('onComplete');
        await this.onFinishGoal()
      },
      onFinishGoal: async () => {
        console.log('onFinishGoal')
      }
    }

  }

  onFinishGoal = async()=>{
    await this.opacityElements(0, this.removeElements);
  };

  removeElements = async()=>{
    this._elements.forEach(item=>{
      this.scene.remove(item)
    });

    this._elements = [];

    await this.callback('onFinishGoal')
  };

  callback = async(key) => {
    await this.callbacks[key]()
  };

  setupCallback = async (key, func) =>{
    this.callbacks[key] = func
  };

  addElementToScene = async(item) => {
    this._elements.push(item);
    this.scene.add(item);
    if(item.sibling){
      this._elements.push(item.sibling);
      this.scene.add(item.sibling);
    }
  };

  start = async (data) => {
    this.data = data;
    this._isUseControl  = false;

    let _elements = [...this.data.elements];

    if(this.data.cameraPosition){
      let tt = new TimelineMax({
        onComplete: () => {
          this._isUseControl = true;
          this.callback('onCompleteGoal');
          console.log('onComplete goal position')
        }
      });
      tt.to(this.camera.position, 3, {
        x: this.data.cameraPosition[0],
        y: this.data.cameraPosition[1],
        z: this.data.cameraPosition[2],
        onUpdate: () => {
          // TODO in gate (goalkeeper)
          this.camera.lookAt(this.scene.children[0].position)
        }
      }, 0);
    }

    // this.camera.position.set(...[4.612905080791105, 1042.087865916811, 22.881079531517383]);


    // in constant, maybe
    let ball = {
      "id": ID_BALL,
      "type":"ball",
      "positions":[0, -20, 0]
    };
    _elements.push(ball);
    /**
     * Set elements
     * @type {Promise<*|undefined>[]}
     */
    const el_promises = _elements.map((item) => ElementFactory.create(item, this.data.color));
    let elements = await Promise.all(el_promises);

    /**
     * Set tween animation
     * @type {(number | Promise<void>)[]}
     */
    const tw_promises = elements.map((item) => item && this.addElementToScene(item));
    await Promise.all(tw_promises);

    //await this.opacityElements(1, this.setupAnimation);

    await this.setupAnimation();
    // await this.setupAnimationTest();
  };

  spreadArr = (dict) => {
    if(!dict)
      return dict;
    if('player' in dict && dict.player)
      dict.player = this._elements.find(a=> a.userData && a.userData.id === dict.player);
    if('line' in dict && dict.line)
      dict.line = this._elements.find(a=> a.userData && a.userData.id === dict.line);
    if('ball' in dict && dict.ball)
      dict.ball = this._elements.find(a=> a.userData && a.userData.id === ID_BALL);
    return dict
  };

  onRepeatAnimation = async() => {
    console.log('onRepeatAnimation');
    this._elements.filter(a=>a.userData.type && a.userData.type === TYPE_LINE).forEach(item=>{
      item.geometry.setDrawRange(0, 0)
    })
  };

  setupAnimation = async() => {
    if(this._timeline){
      this._timeline.remove();
      this._timeline = null;
    }
    let t0 = new TimelineMax({
      onComplete: await this.callbacks['onCompleteGoal'],
      onRepeat: await this.onRepeatAnimation,
      repeat: -1,
      repeatDelay: ANIMATION_REPEAT_TIMEOUT
    });
    this._timeline = t0;

    let _points = this.data.points;

    let points = _points.map(item=>{
      return {
        type: 'point',
        positions: [item.pos],
        ...this.spreadArr(item)
      }
    });

    const el_promises = points.filter(a=>!a.rival).map((item) => ElementFactory.create(item, this.data.color));
    let elements = await Promise.all(el_promises);
    const tw_promises = elements.map((item) => item && this.addElementToScene(item));
    await Promise.all(tw_promises);

    /// TEST TO CENTER IN PLAYER
    let t1 = new TimelineMax({repeat: -1});
    this._elements.filter(a=>a.sibling).reduce(function(promise, item) {
      item.sibling.userData.scaleCircle = 1;
      return promise.then(function(result) {
        return t1.to(item.sibling.userData, .5, { // it's ball to small after high
          scaleCircle: 0.5,
          onUpdate:function() {
            item.sibling.scale.set(item.sibling.userData.scaleCircle, 0.1, item.sibling.userData.scaleCircle)
          },
        }).to(item.sibling.userData, .5, { // it's ball to small after high
          scaleCircle: 1,
          onUpdate:function() {
            item.sibling.scale.set(item.sibling.userData.scaleCircle, 0.1, item.sibling.userData.scaleCircle)
          },
        })
      })
    }, Promise.resolve());
    /// END TEST TO CENTER IN PLAYER

    //  TEST ball
    let ball = this._elements.find(a=> a.userData && a.userData.id === ID_BALL);
    // ball.scale.set(1, .1, 1);
    this.camera.lookAt(ball);

    points.sort((a, b)=>a.sort - b.sort).filter(a=>!a.rival).reduce(function(promise, item, index) {
      return promise.then(function(result) {
        item._animBall = false;  // Временно
        item._animPlayer = false;  // Временно

        if(!item.line){
          return
        }

        item.value = 0;
        item.targetValue = 999;
        return t0.to(item, item.durationTime, {
          value: item.targetValue,
          onUpdateParams: ["{self}"],
          onUpdate: (self) => {

            item.line.geometry.setDrawRange(0, item.value);

            let point = Level.toPointFloat(item.value);
            let pos = item.line.curve.getPoint(point);

            if (item.player && !item.static) { // direction with player
              item.player.sibling.position.set(pos.x, pos.y, pos.z);
              item.player.position.set(pos.x, pos.y, pos.z);
            }

            if (item.ball){
              item.ball.position.set(pos.x, pos.y, pos.z);
            }
          },
        }, item.startTime)
      })
    }, Promise.resolve());
  };

  opacityElements = async(opacity=0, callback) =>{
    let t1 = new TimelineMax({
      onComplete: callback
    });
    // hard hook
    this._elements.filter(a=>a.userData.type && a.userData.kind === KIND_LINE_RUN).forEach(item=>{
      item.geometry.setDrawRange(0, 0)
    });
    this._elements.reduce(function(promise, item) {
      return promise.then(function(result) {
        return t1.to(item.material, 1, {
          opacity: opacity
        }, 0)
      })
    }, Promise.resolve());
  };

  static toPointFloat = (value) =>{ /// WTF gavno
    let num = "0000".substring(parseInt(value).toString().length, 3) + parseInt(value);
    return parseFloat(`0.${num}`);
  };
}

export default Game