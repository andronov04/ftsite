import * as THREE from "three";

class Ball {
  constructor(data, color) {
    this.data = data;
    this.color = color;
  }

  create = async() => {
    let geometry = new THREE.SphereGeometry(5, 32, 32);
    let material = new THREE.MeshBasicMaterial({
      color: this.color && this.color.ball || 0xffffff,
      transparent: true
    });
    let sphere = new THREE.Mesh(geometry, material);
    sphere.position.set(...this.data.positions);
    sphere.userData.id = this.data.id;
    sphere.userData.type = 'ball';

    sphere.material.morphTargets = true;

    return sphere
  };

}

export default Ball