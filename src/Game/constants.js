export const LENGTH_CURVE_POINTS = 1000;
export const ANIMATION_REPEAT_TIMEOUT = 3;
export const TYPE_LINE = 'line';
export const KIND_LINE_RUN = 'lineRun';
