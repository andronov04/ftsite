import os
import subprocess

from pathlib import Path

def main():
    p = Path()
    goal_path = p.absolute().joinpath('data/ALLGOALS')
    goalm_path = p.absolute().joinpath('data/ALLGOALSMP4')
    logo_path = p.absolute().joinpath('data/logos.png')
    font_path = p.absolute().joinpath('data/Teko-Regular.ttf')
    for f in goal_path.glob('*.webm'):
      uuid = f.stem
      output = '{}/{}.mp4'.format(goalm_path.absolute(), uuid)
      cmd = "ffmpeg -y -i {input} -i {logo_path} -filter_complex  " \
            "\"overlay=80:80,drawtext=fontfile={font_path}:" \
            "text='WHOSCOREDTHEGOAL.COM': fontcolor=white@.5: fontsize=38: x=80: y=H-th-80\" " \
            "-codec:a copy  {output}".format(input=f.absolute(), logo_path=logo_path, font_path=font_path,
                                             output=output)
      a = subprocess.check_output(cmd, shell=True)


if __name__ == '__main__':
    main()
