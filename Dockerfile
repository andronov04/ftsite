FROM node:alpine as builder
WORKDIR '/app'
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build


FROM nginx:alpine

ENV HTPASSWD='admin:$apr1$k8V014WE$9qbvOj7YCJpOxFn7mUBHa/' \
    FORWARD_PORT=80 \
    FORWARD_HOST=web

WORKDIR /opt

COPY --from=builder /app/dist /usr/share/nginx/html

COPY conf/* ./

RUN apk add --no-cache gettext && chmod +x /opt/docker-entrypoint.sh

CMD ["./docker-entrypoint.sh"]

EXPOSE 80
