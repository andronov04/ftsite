PACKAGE = ftsite
TAG = $(shell cat VERSION)
IMAGE_REGISTRY = docker.io
IMAGE_PREFIX = andronov04/$(PACKAGE)
IMAGE_NAME = $(IMAGE_PREFIX):$(TAG)
IMAGE_NAME_TAGGED = $(IMAGE_PREFIX):$(TAG)
IMAGE_NAME_LATEST = $(IMAGE_PREFIX):latest
PORT = 8080
default: help

title:
	@echo "\nWHO SCORED THE GOAL?\n"

help: title
	@echo "Usage: make {command}"
	@echo "    bumpversion      bump the version"
	@echo "    image            build the image"
	@echo "    clean            clean sources and delete the image"
	@echo "    rmdi             remove unused Docker's images"
	@echo "    prune            clean Docker environment"
	@echo

clean:
	@find . -type d -name '__pycache__'

bumpversion:
	@bumpversion --commit --tag patch

image:
	$(eval SOURCE_VERSION := $(shell cat VERSION))
	$(eval SOURCE_COMMIT := $(shell git rev-parse HEAD))
	docker build --no-cache \
		-t $(IMAGE_NAME_TAGGED) \
		-t $(IMAGE_NAME_LATEST) \
		-f Dockerfile .
	docker tag $(IMAGE_NAME_LATEST) $(IMAGE_REGISTRY)/$(IMAGE_NAME_LATEST)
	docker tag $(IMAGE_NAME_TAGGED) $(IMAGE_REGISTRY)/$(IMAGE_NAME_TAGGED)
	docker push $(IMAGE_REGISTRY)/$(IMAGE_NAME_LATEST)
	docker push $(IMAGE_REGISTRY)/$(IMAGE_NAME_TAGGED)

run: # image
	@docker run --name $(PACKAGE) -it --rm \
		-p $(PORT):$(PORT) \
		$(IMAGE_NAME)

prune:
	@echo "Removing unused Docker images and containers ..."
	@docker system prune

GOALDIR = ${PWD}/data/goals
convert_list: $(GOALDIR)/*
	for file in $^ ; do \
	  echo "Hello"  $${FILE_NAME} $$(basename $${file}) ; \
		ffmpeg -i $${file} -i ${PWD}/public/assets/media/logos.png  -filter_complex  "overlay=80:80,drawtext=fontfile=${PWD}/public/assets/fonts/Teko-Regular.ttf:text='WHOSCOREDTHEGOAL.COM': fontcolor=white@.5: fontsize=38: x=80: y=H-th-80" -codec:a copy  ${PWD}/data/goal_e.mp4 ;\
	done

convert_video:
	@echo "Convert video..."
	ffmpeg -i ${PWD}/data/goal_e.webm -i ${PWD}/public/assets/media/logos.png  -filter_complex  \
	"overlay=80:80, \
	drawtext=fontfile=${PWD}/public/assets/fonts/Teko-Regular.ttf: \
	text='WHOSCOREDTHEGOAL.COM': fontcolor=white@.5: fontsize=38: x=80: y=H-th-80" \
	-codec:a copy  ${PWD}/data/goal_e.mp4

convert_video_text:
	@echo "Convert video text..."
	ffmpeg -i ${PWD}/data/input.webm -vf \
	"[in]drawtext=fontfile=${PWD}/public/assets/fonts/Teko-Regular.ttf: text='WHO': fontcolor=white: fontsize=80: x=50: y=50, \
	drawtext=fontfile=${PWD}/public/assets/fonts/Teko-Regular.ttf: text='SCORED': fontcolor=white: fontsize=80: x=50: y=120, \
	drawtext=fontfile=${PWD}/public/assets/fonts/Teko-Regular.ttf: text='THE': fontcolor=white: fontsize=80: x=50: y=190, \
	drawtext=fontfile=${PWD}/public/assets/fonts/Teko-Regular.ttf: text='GOAL?': fontcolor=white: fontsize=80: x=50: y=260, \
	drawtext=fontfile=${PWD}/public/assets/fonts/Teko-Regular.ttf: text='WHOSCOREDTHEGOAL.COM': fontcolor=white@.5: fontsize=38: x=50: y=350" \
	-codec:a copy  ${PWD}/data/output.mp4

convert_gif:
	@echo "Convert GIF..."
	ffmpeg -i ${PWD}/data/output.mp4 -filter_complex "[0:v] fps=30,scale=w=828:h=-1,split [a][b];[a] palettegen [p];[b][p] paletteuse" ${PWD}/data/output.gif
